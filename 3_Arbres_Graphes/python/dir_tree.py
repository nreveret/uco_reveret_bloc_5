from Arbres_complet import Arbre
import os
from pilesFiles import Pile


def fichiers(adresse) :
    """
    Renvoie la liste des fichiers contenus par le dossier désigné par son adresse
    """
    return [d for d in os.listdir(adresse) if os.path.isfile(adresse+d)]

def dossiers(adresse) :
    """
    Renvoie la liste des dossiers contenus par le dossier désigné par son adresse
    """
    return [d for d in os.listdir(adresse) if os.path.isdir(adresse+d)]

def arbre_dossiers(adresse : str, source : str = "") -> list:
    """
    Fonction récursive renvoyant la liste des enfants (seulement les dossiers) d'un arbre issu du dossier indiqué en argument
    """
    enfants = []

    dossiers_enCours = dossiers(source+adresse+"/")

    for sous_dossier in dossiers_enCours :
        pass

    return enfants

def arbre_complet(adresse : str, source : str= "") -> list :
    """
    Fonction récursive renvoyant la liste des enfants (dossiers et fichiers) d'un arbre issu du dossier indiqué en argument
    """
    pass


if __name__ == "__main__":
    dossier = input("Quel dossier souhaitez-vous explorer ?")

    """
    print(os.listdir(dossier))
    print(fichiers(dossier))
    print(dossiers(dossier))
    """
    arbre = Arbre(dossier, arbre_complet(dossier))
    #arbre.dessiner()
    print(arbre)