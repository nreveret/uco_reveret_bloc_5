from pilesFiles import Element, Pile
from Arbres_binaires_complet import Arbre_binaire


def in_to_post(chaine: str) -> list:
    """
    Traduit un calcul infixe en postfixe à l'aide de l'algorithme 
    de la gare de triage (shunting-yard)

    chaine : un calcul en infixe (str)

    Renvoie une liste contenant les éléments en postfix
    """

    # On ôte les caratères " "
    chaine = chaine.replace(" ", "")
    # On ôte les caratères "," et on met des "." à la place
    chaine = chaine.replace(",", ".")

    q = []
    p = Pile()

    # L'ordre de priorité
    priorites = {"-": 1,
                 "+": 1,
                 "/": 2,  # 2?
                 ":": 2,
                 "*": 2,
                 "^": 3,
                 "r": 3,  # La racine carrée
                 "(": -1,
                 ")": -1}

    # Règles d'associativité
    associativite = {"-": "g",
                     "+": "g",
                     "/": "g",
                     ":": "g",
                     "*": "g",
                     "^": "d",
                     "r": 4,  # La racine carrée
                     }

    i = 0
    while i < len(chaine):
        # Le caractère à lire
        token = chaine[i]
        # Gestion des nombres >=10 et décimaux
        if token.isnumeric():
            while (i+1 < len(chaine)) and (chaine[i+1].isdigit() or chaine[i+1] == "."):
                i += 1
                token += chaine[i]
            try:
                float(token)
            except:
                Exception(f"Le \"nombre\" {token} est mal tapé")
            q.append(token)
        # Gestion des opérateurs
        elif token in ["+", "-", "*", "/", ":", "r", "^"]:
            while not p.est_vide():
                ope = p.depiler()
                if (associativite[token] == "g" and priorites[ope] >= priorites[token]) or (associativite[token] == "d" and priorites[ope] > priorites[token]):
                    q.append(ope)
                else:
                    p.empiler(ope)
                    break
            p.empiler(token)
        # Gestion de la parenthèse ouvrante
        elif token == "(":
            p.empiler(token)
        # Gestion de la parenthèse fermante
        elif token == ")":
            while not p.est_vide():
                ope = p.depiler()
                if ope != "(":
                    q.append(ope)
                else:
                    break
        # On passe au caractère suivant
        i += 1

    while not p.est_vide():
        q.append(p.depiler())

    return q


def post_to_tree(expression: list) -> Arbre_binaire:
    """
    Traduit une expression en postfixe en un arbre
    expr est une expression en postfixe
    Renvoie l'arbre des opérations à efectuer
    """
    pass

if __name__ == "__main__":
    expression = "(9-r(3+2))/(5+3*8)^2"

    print(f"L'expression infixe à calculer  : {expression}")
    postfixe = in_to_post(expression)
    print(f"L'expression postfixe à calculer  : {' '.join(postfixe)}")
    
    """
    arbre = post_to_tree(postfixe)
    print(arbre)
    arbre.dessiner()
    """