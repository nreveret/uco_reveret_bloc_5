import graphviz
from pilesFiles import Pile, File


class ABR:
    """
    Classe implémentant un arbre binaire de recherche dont les noeuds sont caractérisés par 
    - une valeur (de type quelconque)
    - une fils gauche et un fils droit
    Les enfants gauche et droit sont eux-mêmes des arbres
    """

    def __init__(self, valeur=None, gauche=None, droit=None):
        """
        Constructeur
        """
        self.valeur = valeur
        self.gauche = gauche
        self.droit = droit

    def inserer(self, valeur):
        """
        Insère la valeur dans l'arbre
        """
        pass

    def contient(self, valeur):
        """
        Renvoie True si valeur est dans l'arbre, False dans le cas contraire
        """
        pass

    def maximum(self):
        """
        Renvoie la valeur maximale de cet arbre : c'est l'élément le plus à droite
        """
        pass

    def minimum(self):
        """
        Renvoie la valeur minimale de cet arbre : c'est l'élément le plus à gauche
        """
        pass

    def get_valeur(self):
        """
        Accesseur de la valeur de l'arbre
        """
        return self.valeur

    def set_valeur(self, valeur):
        """
        Mutateur de la valeur de l'arbre
        """
        self.valeur = valeur

    def get_gauche(self):
        """
        Accesseur du fils gauche
        """
        return self.gauche

    def set_gauche(self, arbre):
        """
        Mutateur du fils gauche
        """
        self.gauche = arbre

    def get_droit(self):
        """
        Accesseur du fils droit
        """
        return self.droit

    def set_droit(self, arbre):
        """
        Mutateur du fils droit
        """
        self.droit = arbre

    def est_feuille(self) -> bool:
        """
        Renvoie True si l'arbre est une feuille (pas d'enfants)
        False dans le cas contraire
        """
        return self.gauche == None and self.droit == None

    def taille(self) -> int:
        """
        Renvoie la taille de l'arbre (le nombre de noeud)
        """
        t = 0
        if self.valeur != None:
            t += 1

        if self.gauche:
            t += self.gauche.taille()
        if self.droit:
            t += self.droit.taille()

        return t

    def hauteur(self) -> int:
        """
        Renvoie la hauteur de l'arbre
        Renvoie 1 si l'arbre est une feuille
        """
        h = 1

        hauteurs = [0]

        if self.gauche:
            hauteurs.append(self.gauche.hauteur())
        if self.droit:
            hauteurs.append(self.droit.hauteur())

        return h + max(hauteurs)

    def parcours_largeur(self) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en largeur
        """
        if self.valeur == None:
            return "L'arbre est vide"

        p = ""

        f = File()

        f.enfiler(self)

        while not f.est_vide():
            enCours = f.defiler()
            p += (str(enCours.valeur) + " -> ")
            if enCours.gauche:
                f.enfiler(enCours.gauche)
            if enCours.droit:
                f.enfiler(enCours.droit)

        return p[:-4]

    def parcours_profondeur_prefixe(self, depart=True) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en profondeur prefixe
        """
        if self.valeur == None:
            return ""
        else:
            p = ""

            p += str(self.valeur) + " -> "

            if self.gauche:
                p += self.gauche.parcours_profondeur_prefixe(False)

            if self.droit:
                p += self.droit.parcours_profondeur_prefixe(False)

            if depart:
                return p[:-4]
            else:
                return p

    def parcours_profondeur_postfixe(self, depart=True) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en profondeur postfixe
        """
        if self.valeur == None:
            return ""
        else:
            p = ""

            if self.gauche:
                p += self.gauche.parcours_profondeur_postfixe(False)

            if self.droit:
                p += self.droit.parcours_profondeur_postfixe(False)

            p += str(self.valeur) + " -> "

            if depart:
                return p[:-4]
            else:
                return p

    def parcours_profondeur_infixe(self, depart=True) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en profondeur infixe
        """
        if self.valeur == None:
            return ""
        else:
            p = ""

            if self.gauche:
                p += self.gauche.parcours_profondeur_infixe(False)

            p += str(self.valeur) + " -> "

            if self.droit:
                p += self.droit.parcours_profondeur_infixe(False)

            if depart:
                return p[:-4]
            else:
                return p

    def dessiner(self) -> None:
        """
        Représente l'arbre
        """
        key = 0
        pile = Pile()

        g = graphviz.Graph(format="png")
        g.node(str(key), str(self.valeur))
        pile.empiler((self, key))

        while not pile.est_vide():
            enCours, enCours_key = pile.depiler()
            if enCours.gauche:
                key += 1
                g.node(str(key), str(enCours.gauche.valeur))
                pile.empiler((enCours.gauche, key))
                g.edge(str(enCours_key), str(key))
            else:
                key += 1
                g.node(str(key), style='invis')
                g.edge(str(enCours_key), str(key), style="invis", weight="20")
            if enCours.droit:
                key += 1
                g.node(str(key), str(enCours.droit.valeur))
                pile.empiler((enCours.droit, key))
                g.edge(str(enCours_key), str(key))
            else:
                key += 1
                g.node(str(key), style='invis')
                g.edge(str(enCours_key), str(key), style="invis", weight="20")

        g.render(view=True)

        return None

    def __str__(self):
        """
        Représentation de l'arbre dans la console
        """
        h_barre = "_____"

        s = ""
        if self.valeur == None:
            s = "L'arbre est vide"
        else:
            depth = 0
            p = Pile()
            p.empiler((self, depth))
            while not p.est_vide():
                enCours, depth = p.depiler()
                if depth > 0:
                    s += "  |"+(" "*8+"|")*(depth-1) + h_barre + \
                        str(enCours.get_valeur()) + "\n"
                elif depth == 0:
                    s += str(enCours.get_valeur()) + "\n"
                if enCours.gauche:
                    p.empiler((enCours.gauche, depth+1))
                if enCours.droit:
                    p.empiler((enCours.droit, depth+1))

        return s


if __name__ == "__main__":
    from random import randint

    abr = ABR(randint(0, 20))

    for i in range(10):
        abr.inserer(randint(0, 20))

    abr.dessiner()
    print(abr)
    print("La taille de l'arbre est :", abr.taille())
    print("La hauteur de l'arbre est :", abr.hauteur())
    print("Le parcours en largeur de l'arbre est :",
          abr.parcours_largeur())
    print("Le parcours en profondeur prefixe de l'arbre est :",
          abr.parcours_profondeur_prefixe())
    print("Le parcours en profondeur infixe de l'arbre est :",
          abr.parcours_profondeur_infixe())
    print("Le parcours en profondeur postfixe de l'arbre est :",
          abr.parcours_profondeur_postfixe())
    n = randint(0, 20)
    print("L'abre contient", n, "? :", abr.contient(n))
