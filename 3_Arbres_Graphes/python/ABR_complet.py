import graphviz
from pilesFiles import Pile, File


class ABR:
    """
    Classe implémentant un arbre binaire de recherche dont les noeuds sont caractérisés par 
    - une valeur (de type quelconque)
    - une fils gauche et un fils droit
    Les enfants gauche et droit sont eux-mêmes des arbres
    """

    def __init__(self, valeur=None, gauche=None, droit=None):
        """
        Constructeur
        """
        if valeur != None:
            self.valeur = valeur
            if gauche:
                self.gauche = gauche
            else:
                self.gauche = None
            if droit:
                self.droit = droit
            else:
                self.droit = None
        else:  # arbre vide
            self.valeur = None
            self.gauche = None
            self.droit = None

    def get_valeur(self):
        """
        Accesseur de la valeur de l'arbre
        """
        return self.valeur

    def set_valeur(self, valeur):
        """
        Mutateur de la valeur de l'arbre
        """
        self.valeur = valeur

    def get_gauche(self):
        """
        Accesseur du fils gauche
        """
        return self.gauche

    def set_gauche(self, arbre):
        """
        Mutateur du fils gauche
        """
        self.gauche = arbre

    def get_droit(self):
        """
        Accesseur du fils droit
        """
        return self.droit

    def set_droit(self, arbre):
        """
        Mutateur du fils droit
        """
        self.droit = arbre

    def est_feuille(self) -> bool:
        """
        Renvoie True si l'arbre est une feuille (pas d'enfants)
        False dans le cas contraire
        """
        return self.gauche == None and self.droit == None

    def taille(self) -> int:
        """
        Renvoie la taille de l'arbre (le nombre de noeud)
        """
        t = 0
        if self.valeur != None:
            t += 1

        if self.gauche:
            t += self.gauche.taille()
        if self.droit:
            t += self.droit.taille()

        return t

    def hauteur(self) -> int:
        """
        Renvoie la hauteur de l'arbre
        Renvoie 1 si l'arbre est une feuille
        """
        h = 1

        hauteurs = [0]

        if self.gauche:
            hauteurs.append(self.gauche.hauteur())
        if self.droit:
            hauteurs.append(self.droit.hauteur())

        return h + max(hauteurs)

    def parcours_largeur(self) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en largeur
        """
        if self.valeur == None:
            return "L'arbre est vide"

        p = ""

        f = File()

        f.enfiler(self)

        while not f.est_vide():
            enCours = f.defiler()
            p += (str(enCours.valeur) + " -> ")
            if enCours.gauche:
                f.enfiler(enCours.gauche)
            if enCours.droit:
                f.enfiler(enCours.droit)

        return p[:-4]

    def parcours_profondeur_prefixe(self, depart=True) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en profondeur prefixe
        """
        if self.valeur == None:
            return ""
        else:
            p = ""

            p += str(self.valeur) + " -> "

            if self.gauche:
                p += self.gauche.parcours_profondeur_prefixe(False)

            if self.droit:
                p += self.droit.parcours_profondeur_prefixe(False)

            if depart:
                return p[:-4]
            else:
                return p

    def parcours_profondeur_postfixe(self, depart=True) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en profondeur postfixe
        """
        if self.valeur == None:
            return ""
        else:
            p = ""

            if self.gauche:
                p += self.gauche.parcours_profondeur_postfixe(False)

            if self.droit:
                p += self.droit.parcours_profondeur_postfixe(False)

            p += str(self.valeur) + " -> "

            if depart:
                return p[:-4]
            else:
                return p

    def parcours_profondeur_infixe(self, depart=True) -> None:
        """
        Renvoie la chaîne de caractère formée par les valeurs
        des noeuds rencontrés lors d'un parcours en profondeur infixe
        """
        if self.valeur == None:
            return ""
        else:
            p = ""

            if self.gauche:
                p += self.gauche.parcours_profondeur_infixe(False)

            p += str(self.valeur) + " -> "

            if self.droit:
                p += self.droit.parcours_profondeur_infixe(False)

            if depart:
                return p[:-4]
            else:
                return p

    def dessiner(self) -> None:
        """
        Représente l'arbre
        """
        key = 0
        pile = Pile()

        g = graphviz.Graph(format="png")
        g.node(str(key), str(self.valeur))
        pile.empiler((self, key))

        while not pile.est_vide():
            enCours, enCours_key = pile.depiler()
            if enCours.gauche:
                key += 1
                g.node(str(key), str(enCours.gauche.valeur))
                pile.empiler((enCours.gauche, key))
                g.edge(str(enCours_key), str(key))
            else:
                key += 1
                g.node(str(key), style='invis')
                g.edge(str(enCours_key), str(key), style="invis", weight="20")
            if enCours.droit:
                key += 1
                g.node(str(key), str(enCours.droit.valeur))
                pile.empiler((enCours.droit, key))
                g.edge(str(enCours_key), str(key))
            else:
                key += 1
                g.node(str(key), style='invis')
                g.edge(str(enCours_key), str(key), style="invis", weight="20")

        g.render(view=True)

        return None

    def __str__(self):
        """
        Représentationd dee l'arbre dans la console
        """
        h_barre = "_____"

        s = ""
        if self.valeur == None:
            s = "L'arbre est vide"
        else:
            depth = 0
            p = Pile()
            p.empiler((self, depth))
            while not p.est_vide():
                enCours, depth = p.depiler()
                if depth > 0:
                    s += "  |"+(" "*8+"|")*(depth-1) + h_barre + \
                        str(enCours.get_valeur()) + "\n"
                elif depth == 0:
                    s += str(enCours.get_valeur()) + "\n"
                if enCours.gauche:
                    p.empiler((enCours.gauche, depth+1))
                if enCours.droit:
                    p.empiler((enCours.droit, depth+1))

        return s

    def inserer(self, valeur):
        """
        Insère la valeur dans l'ABR
        Si la valeur est déjà dans l'arbre elle n'est pas insérée
        """
        if self.valeur == None :
            self.valeur = valeur
        else :
            if valeur <= self.valeur:
                if self.gauche:
                    self.gauche.inserer(valeur)
                else:
                    self.gauche = ABR(valeur)
            else:
                if self.droit:
                    self.droit.inserer(valeur)
                else:
                    self.droit = ABR(valeur)

    def contient(self, valeur):
        """
        Renvoie True si valeur est dans l'arbre, False dans le cas contraire
        """
        if self.valeur == None:
            return False
        elif self.valeur == valeur:
            return True
        elif valeur < self.valeur:
            if not self.gauche:
                return False
            else:
                return self.gauche.contient(valeur)
        elif valeur > self.valeur:
            if not self.droit:
                return False
            else:
                return self.droit.contient(valeur)

    def maximum(self):
        """
        Renvoie la valeur maximale de cet arbre : c'est l'élément le plus à droite
        """
        if not self.droit:
            return self.valeur
        else:
            if self.droit:
                return self.droit.maximum()

    def minimum(self):
        """
        Renvoie la valeur minimale de cet arbre : c'est l'élément le plus à gauche
        """
        if not self.gauche:
            return self.valeur
        else:
            if self.gauche:
                return self.gauche.minimum()

    def supprimer(self, valeur):
        """
        Supprime le noeud contenant cette valeur
        Dans le cas ou le noeud est une feuille, il est simplement supprimé
        Dans le cas où il possède un seul enfant, il est remplacé par celui-ci
        S'il possède deux enfants on le remplace aléatoirement par le fils gauche ou droit
        """
        from random import random

        if self.valeur == None:
            raise Exception("La valeur n'est pas dans l'arbre")
        else:
            if self.valeur == valeur:
                print("on a trouvé")
                """
                if self.est_feuille():
                    print("feuille")
                    self.valeur = None
                else:
                """
                if self.gauche and not self.droit:
                    print("gauche simple")
                    self.valeur = self.gauche.valeur
                    if self.gauche.gauche :
                        self.gauche = self.gauche.gauche
                    if self.gauche.droit :
                        self.droit = self.gauche.droit
                elif self.droit and not self.gauche:
                    print("droit simple")
                    self.valeur = self.droit.valeur
                    if self.droit.gauche :
                        self.gauche = self.droit.gauche
                    if self.droit.droit :
                        self.droit = self.droit.droit
                else:
                    print("gauche et droit")
                    if random() < 0.5:  # on remplace la valeur par le successeur le plus proche
                        # -> le minimum du sous-arbre droit
                        succ = self.droit.minimum()
                        self.valeur = succ
                        return self.droit.supprimer(succ)
                    else:  # on remplace la valeur par le prédécesseur le plus proche
                        # -> le maximum du sous-arbre gauche
                        pred = self.gauche.maximum()
                        self.valeur = pred
                        return self.gauche.supprimer(pred)
            elif valeur < self.valeur:
                if not self.gauche:
                    raise Exception("La valeur n'est pas dans l'arbre")
                else:
                    print("on descend à gauche")
                    return self.gauche.supprimer(valeur)
            elif valeur > self.valeur:
                if not self.droit:
                    raise Exception("La valeur n'est pas dans l'arbre")
                else:
                    print("on descend à droite")
                    return self.droit.supprimer(valeur)
            elif self.gauche and self.gauche.valeur == valeur :
                self.gauche = None
            elif self.droit and self.droit.valeur == valeur :
                self.droit = None


if __name__ == "__main__":
    from random import randint

    abr = ABR(randint(0, 20))

    for i in range(20):
        abr.inserer(randint(0, 20))
        # abr.inserer(i)


    abr.dessiner()
    print(abr)
    print("La taille de l'arbre est :", abr.taille())
    print("La hauteur de l'arbre est :", abr.hauteur())
    print("Le parcours en largeur de l'arbre est :",
          abr.parcours_largeur())
    print("Le parcours en profondeur prefixe de l'arbre est :",
          abr.parcours_profondeur_prefixe())
    print("Le parcours en profondeur infixe de l'arbre est :",
          abr.parcours_profondeur_infixe())
    print("Le parcours en profondeur postfixe de l'arbre est :",
          abr.parcours_profondeur_postfixe())
    print("Le minimum est :", abr.minimum())
    print("Le maximum est :", abr.maximum())
    n = randint(0, 20)
    print("L'abre contient", n, "? :", abr.contient(n))
"""    print("On supprimme la valeur cherchée ci-dessus...")
    abr.supprimer(n)
    print(abr)
    abr.dessiner()"""
