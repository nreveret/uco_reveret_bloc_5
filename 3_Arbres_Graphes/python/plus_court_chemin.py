from bretagne import g

def dijkstra(g, depart : str) -> list :
    """
    Met en place l'algorithme de Dijskstra

    g est un graphe représenté par sa matrice d'adjacence
    depart est le nom du sommet de départ

    Renvoie le dictionnaire associant à chaque sommet son prédecesseur dans le plus
    court trajet issue de depart
    """

    pass

def trajet(g, depart : str, arrivee : str) -> list :
    """
    Applique l'algorithme de Dijkstra puis renvoie le chemin entre depart et arrivée
    """
    pass

if __name__ == "__main__":

    print(trajet(g,"Loudéac", "Belle-Ile"))