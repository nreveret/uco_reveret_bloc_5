from bretagne import g

def dijkstra(g, depart : str) -> list :
    """
    Met en place l'algorithme de Dijskstra

    g est un graphe représenté par sa matrice d'adjacence
    depart est le nom du sommet de départ

    Renvoie le dictionnaire associant à chaque sommet son prédecesseur dans le plus
    court trajet issue de depart
    """

    visites = []
    distances = {}
    predecesseurs = {}
    sommets = g.get_sommets()
    for s in sommets :
        distances[s] = float('Inf')
        predecesseurs[s] = None

    distances[depart] = 0

    # On effectue l'algorithme tant que tous les sommets n'ont pas été explorés
    while len(visites) < len(sommets) :
        # Choix du sommet non visité le plus proche
        d_min = float('inf')
        a = -1
        for s in sommets :
            if s not in visites and distances[s] < d_min :
                d_min = distances[s]
                a = s
        
        # On visite ce sommet
        visites.append(a)

        # On recalcule les trajet vers les autres sommets
        for v in g.voisins(a) :
            if distances[v] > distances[a] + g.get_valeur_arete(a,v) :
                distances[v] = distances[a] + g.get_valeur_arete(a,v)
                predecesseurs[v] = a
    
    return(predecesseurs)

def trajet(g, depart, arrivee) -> list :
    p = dijkstra(g, depart)

    trajet = [arrivee]
    d = 0

    enCours = arrivee

    while p[enCours] != depart :
        trajet.append(p[enCours])
        d += g.get_valeur_arete(p[enCours], enCours)
        enCours = p[enCours]

    trajet.append(p[enCours])
    d += g.get_valeur_arete(p[enCours], enCours)

    return trajet[::-1], d

if __name__ == "__main__":

    print(trajet(g,"Loudéac", "Belle-Ile"))