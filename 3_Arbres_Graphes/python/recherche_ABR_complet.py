from ABR_complet import ABR
from random import choice
from time import perf_counter_ns

texte = ""

# Ouverture du texte
with open("guerre_et_paix.txt", "r") as f :
    texte = f.read()

# On met tout en minuscule
texte = texte.lower()

# On supprime la ponctuation en la reamplaçant par des espaces " "
import re

texte = re.sub('[^A-ZÉÈÀÙÊÔÂÜÛÏÎa-zéèàùêôâüûïî]+', ' ', texte)

# La liste des mots du texte, sans les doublons
mots = list(set(texte.split()))
print("La liste compte ",len(mots), "mots différents")
# Création de l'arbre
abr = ABR()
for mot in mots :
    abr.inserer(mot)

# Recherche d'un mot au hasard
mot_a_trouver = choice(mots)
print("On cherche le mot :", mot_a_trouver)

# Recherche dans l'ABR
debut = perf_counter_ns()
abr.contient(mot_a_trouver)
fin = perf_counter_ns()
print("La recherche dans l'ABR a pris", fin-debut, "ns")

# Recherche dans la liste
debut = perf_counter_ns()
mot_a_trouver in mots
fin = perf_counter_ns()
print("La recherche dans la liste (avec 'in') a pris", fin-debut, "ns")


# Recherche dans l'ABR
debut = perf_counter_ns()
abr = ABR()
for mot in mots :
    abr.inserer(mot)
abr.contient(mot_a_trouver)
fin = perf_counter_ns()
print("La création de l'ABR et la recherche dans l'ABR ont pris", fin-debut, "ns")
