---
title: |
    DIU Enseigner la spécialité NSI \
    Bloc 5 \
    - Arbres et Graphes -

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhf{}
    \lhead{DIU Enseigner la spécialité NSI}
    \rhead{Arbres et Graphes}
    \cfoot{\thepage}

author: |
    N. Revéret

lang:
- french

geometry:
- top=2.5cm
- bottom=2cm
- left=2cm
- right=2cm

output:
  pdf_document:
    fig_caption: yes
    table_caption: no
    df_print: kable
  html_document:
    fig_caption: yes
    df_print: paged
---

# Généralités sur les arbres

Un arbre est un type particulier de *graphe*. A ce titre, un arbre est constitué :

* de *sommets* : on parle ici de **nœud**
* d'*arêtes* : les liens entre les sommets

Un arbre est un graphe :

* **non orienté** : les arêtes ne comportent pas de direction, le lien allant d'un sommet *père* à un *enfant* est le même que celui allant de l'*enfant* au *père*
* **connexe** : aucun des sommets n'est isolé, tous font partie de l'arbre
* **non cyclique** : il est interdit d'avoir des boucles dans un arbre

![Un arbre, une forêt (plusieurs arbres), un graphe cyclique](images/contre_exemples.png){width=60%}

Le vocabulaire associé est le suivant (entre parenthèse les traductions en anglais):

* la **racine** (*root*) est le sommet principal. C'est le seul à ne pas avoir de parent
* la **profondeur** (*level*) d'un nœud est sa position du nœud dans l'arbre.C'est la longueur du chemin entre la racine et le nœud
* la **taille** d'un arbre est son nombre de nœud
* le **parent** (*parent*) d'un nœud est le nœud qui le précède directement dans l'arbre
* les **ancêtres** (*ancestors*) d'un nœud sont son parents et les parents de ses parents
* les **enfants** (*children*) d'un nœud sont les nœuds qui lui sont directement reliés au niveau supérieur
* les **descendants** d'un nœud sont ses enfants et les enfants de ses enfants
* des nœuds de même parent sont **frères** (*siblings*)
* un nœud sans enfant est situé en bas de l'arbre : c'est une **feuille** (*leave*)
* un nœud qui n'est pas une feuille est un **nœud intérieur**
* des nœuds de même profondeur sont des **voisins** (*neighbors*)
* la **hauteur** d'un arbre est sa profondeur maximale. Il faut toutefois prendre soin de préciser la définition. Il est en effet possible lorsque l'on compte la profondeur/hauteur d'un arbre de compter :

  * le nombre d'**arêtes** situées sur un chemin reliant deux nœuds. Dans ce cas la racine est à la hauteur $0$
  * le nombre de **nœuds** situés sur un chemin reliant deux nœuds. Dans ce cas la racine est à la hauteur $1$

![Vocabulaire](images/labeled_tree.png){width=50%}

Il existe plusieurs façon de représenter les arbres en informatique. L'une des méthode classique est de les définir de façon **récursive** comme des nœuds pointant vers d'autres nœuds. Ainsi un arbre contient des sous-arbres :

![Arbre et sous-arbre](images/subtree.png){width=40%}

La structure d'une classe `Arbre` peut donc être :

* un pointeur vers son *parent* (pas indispensable)
* des données. On les regroupe dans la `valeur` 
* des `enfants` qui sont eux-mêmes des arbres, stockés dans une liste, éventuellement vide

Dans cette implémentation, on convient qu'un arbre vide est un arbre dont la valeur est vide (`None` en python).

Une autre façon de procéder serait de donner les attributs précédents à une classe `Nœud`. Dans ce cas, on crée un arbre à l'aide d'une classe ne possédant qu'un seul attribut : sa `racine` qui pointe vers le nœud racine de l'arbre. Un arbre vide pointe alors nulle part (`None` là encore).

Afin de créer un sous-arbre-extrait, on choisit un nœud et on crée un arbre en précisant qu'il est la racine.

Le fichier `Arbres_complet.py` contient une implémentation des arbres sans utiliser de classe `Nœud`. La représentation des arbres implémentées ici nécessite d'utiliser *Graphviz* :

* installer tout d'abord le logiciel [Graphviz](https://graphviz.org/)
* installer ensuite le module python *Graphviz* (`pip install graphviz`)

1. Utiliser cette classe afin de construire l'arbre de la figure 3.

# Algorithmes classiques sur les arbres

## Hauteur de l'arbre

On peut calculer la hauteur d'un arbre en utilisant une fonction récursive.

```
hauteur(arbre A)
    Si A est vide :
        Retourner 0
    Sinon :
        hauteurs = [0]
        Pour chaque enfant e de A :
          Ajouter hauteur(Sous-arbre issu de e) à la fin de hauteurs
        Retourner 1 + maximum(hauteurs)
```


## Taille de l'arbre

On rappelle que la taille d'un arbre est égale au nombre de nœuds non-vides qu'il contient. Là encore il est possible de déterminer la taille à l'aide d'une fonction récursive :

```
taille(arbre A)
    Si A est vide :
        Retourner 0
    Sinon :
        t = 1 
        Pour chaque enfant e de A :
          t += taille(Sous-arbre issu de e)
        
        Retourner t
```

## Parcours de l'arbre

Il est souvent utile de parcourir l'ensemble des nœuds d'un arbre, ne serait-ce que pour les compter par exemple. Le parcours peut aussi être utile afin de rechercher une valeur dans un arbre.

On distingue globalement deux méthodes de parcours :

* les parcours en **largeur d'abord**: on visite tous les nœuds d'une certaine profondeur avant de passer à la profondeur suivante
* les parcours en **profondeur d'abord**: on visite un nœud et tous les descendants avant de passer à ses voisins. Il en existe plusieurs

![L'arbre à parcourir](images/parcours.gif){width=50%}

Sur la figure, le parcours en largeur donnera l'ordre suivant (on convient que les nœuds de gauche sont visités avant ceux de droite) :

$$11 \rightarrow 8 \rightarrow 14 \rightarrow 5 \rightarrow 10 \rightarrow 13 \rightarrow 15$$

Un parcours en profondeur pourra donner :

$$11 \rightarrow 8 \rightarrow 5 \rightarrow 10 \rightarrow 14 \rightarrow 13 \rightarrow 15$$

On peut implémenter un parcours en largeur à l'aide d'une *file* :

```
ParcoursLargeur(arbre A):
    f est une file vide
    Enfiler la racine de A dans f
    Tant que la f est non vide :
        Défiler un sommet s de f
        Afficher s
        Pour chaque enfant e de s
            Enfiler e dans f
```

![Parcours en largeur](images/largeur.gif){width=50%}


Les parcours en profondeur peuvent eux être définis de façon récursive. Afin de parcourir un arbre, on parcours successivement tous les sous-arbres extraits à partir des enfants de sa racine.On distingue :

* les parcours *préfixe* dans lesquels le nœud est traîté avant ses enfants
* et les parcours *postfixe* dans lequel il est traité ensuite

```
ParcoursPréfixe(arbre A):
    Afficher la racine de A
    Pour tous les enfants e de la racine :
        ParcoursPréfixe( Sous-Arbre issu de e )
```

![Parcours préfixe : on visite les nœuds lorsque l'on passe à leur gauche](images/prefixe.gif){width=50%}


```
ParcoursPostfixe(arbre A):
    Pour tous les enfants e de la racine :
        ParcoursPostfixe( Sous-Arbre issu e )
    Afficher la racine de A
```

![Parcours postfixe : on visite les nœuds lorsque l'on passe à leur droite](images/postfixe.gif){width=50%}

1. Utiliser un parcours en largeur afin de représenter l'arbre des fichiers et dossiers correspondant à une adresse sur le disque de votre machine (fichier `dir_tree.py`):

```
../
  |_____pandoc_nico.css
  |_____conversions.py
  |_____3_Arbres_Graphes.pdf
  |_____3_Arbres_Graphes.md
  |_____3_Arbres_Graphes.html
  |_____python
  |        |_____pilesFiles.py
  |        |_____dir_tree_complet.py
  |        |_____Arbres_complet.py
  |        |_______pycache__
  |        |        |_____pilesFiles.cpython-38.pyc
  |        |        |_____Arbres_complet.cpython-38.pyc
  |_____images
  |        |_____tree.png
  ...
  ```

# Arbre binaire

## Généralités

Un arbre binaire est un arbre classique à ceci près que chaque nœud possède au plus deux enfants. On parle alors d'enfant *gauche* et *droit*.

On distingue :

* les arbres binaires *pleins* : chaque nœud a soit $0$ soit $2$ enfants
* les arbres binaires *complets* : tous les niveaux sont "remplis" sauf éventuellement le dernier qui est rempli à partir de la gauche
* les arbres binaires *parfaits* : tous les niveaux **sans exception** sont "remplis"

![Arbres binaires](images/arbres_binaires.png){width=80%}

*Remarque :* Il est intéressant de chercher le cas le plus défavorable pour un arbre binaire comptant $n$ nœuds. Il s'agit de l'arbre dans lequel chaque nœud n'a qu'un seul enfant.

![Arbres binaires déséquilibrés](images/degenerate_binary_tree.png){width=35%}

Le cas d'un arbre parfait permet de raisonner sur relation existant entre la hauteur $h$ et la taille $n$ d'un arbre binaire. En effet :

* le premier "niveau" d'un arbre (la racine) compte $1$ seul nœud,
* le deuxième "niveau" compte $2$ nœuds,
* ...
* le  $h$-ième niveau en compte $2^{h-1}$

En tout l'arbre compte donc : $n=1 + 2 + \dots + 2^{h-1}=2^h-1$ nœuds.

*Remarque :* On peut aussi prouver ce résultat en numérotant les nœuds. La racine prend le numéro $1$ puis à chaque fois que l'on descend d'un niveau on rajoute un $1$ si l'on va vers la gauche, un $0$ si l'on va vers la droite. Ainsi les nœuds du deuxième niveau sont numérotés $11$ et $10$, ceux du troisième $111$, $110$, $101$ et $100$... En listant tous les numéros on réalise qu'il s'agit de tous les nombres que l'on peut écrire sur $n$ bits sauf le $0$. Il y en a bien $2^{h}-1$.

Réciproquement, quelle est la hauteur $h$ d'un arbre binaire de taille $n$ ?

Si l'arbre est entièrement déséquilibré (que des fils droits par exemple), la hauteur vaut $n$.

A l'inverse, dans le cas d'un arbre parfait de hauteur $h$, on a $taille=n=2^h-1$. En renversant cette formule, on obtient $h=\log{_2(n+1)}$. On peut retenir $h\approx \log{_2(n)}$.

*Remarque :* Dans le cas d'un arbre binaire déséquilibré au maximum on a $h=n$.

## Implémentation des arbres binaires

Deux implémentations classiques existent :

* une adaptation de la classe `Arbre` dans laquelle les enfants ne sont plus stockés dans une liste mais directement désignés individuellement (attribut `gauche` et `droit`)
* un codage dans un tableau de taille égale à celle du tableau. Dans ce cas, on stocke la racine dans la première cellule. On convient ensuite que les enfants gauche et droit d'un nœud stockés dans la cellule $n$ sont eux stockés dans les cellules $2n+1$ et $2n+2$ (dans le cas où les indices débutent à $0$, $2n$ et $2n+1$ si les indices débutent à $1$)

![Arbre binaire dans un tableau (les indices débutent à $1$)](images/binary_tree_array.png){width=70%}

## Algorithmes classiques des arbres binaires

Les algorithmes des arbres binaires sont identiques à ceux des arbres classiques avec une différence : lors de l'étude des enfants d'un nœud, on ne parcourt pas la liste dans une boucle *Pour* mais on teste individuellement les enfants gauche et droit.

On peut aussi rajouter un parcours : le parcours en profondeur *préfixe* dans lequel un nœud est étudié entre l'étude de ses enfants gauche et droit.

```
ParcoursInfixe(Arbre Binaire A):
    ParcoursInfixe( Sous-Arbre gauche de la racine de A )
    Afficher la racine de A
    ParcoursInfixe( Sous-Arbre droit de la racine de A )
```

![Parcours infixe : on visite les nœuds lorsque l'on passe en dessous](images/infixe.gif){width=50%}

1. Compléter le fichier (et la classe) `Arbres_binaires.py`.

Dans une expression mathématique telle que $2+5\times \sqrt{7}$, il y a trois opérandes ($2$, $5$ et $\sqrt{7}$) et trois opérateurs ($+$, $\times$ et $\sqrt{.}$). Ces opérandes n'ont pas la même *valence* :

* $+$ et $\times$ opèrent sur $2$ nombres
* $\sqrt{.}$ opère sur $1$ seul nombre

Une fonction de trois variables aurait ainsi une valence de $3$... 

Considérons une expression mathématique dans laquelle on n'utilise que des opérateurs de valence inférieure ou égale à 2 : les opérations classiques et l'exponentiation ($+$, $-$, $\times$, $/$ et $\hat{\,}$), tous de valence égale à $2$, et la racine carrée ($\sqrt{.}$ que l'on notera `r` dans le code), de valence égale à $1$.

L'algorithme de la *gare de triage* (*shunting-yard*, voir le cours sur les piles et les files) permet de transformer un calcul exprimé en notation *infixe* en une expression *postfixe*. Ce code est donné dans le fichier `arbre_calcul.py` (fonction `in_to_post`). 

2. Compléter la fonction `post_to_tree` créant l'arbre illustrant un calcul en notation *postfixe*.

![L'arbre du calcul $12+(15-3\times 2)\times 5$](images/calcul.png){width=30%}

## Arbres binaires de recherche

Un arbre binaire de recherche (*ABR*) est un arbre binaire contenant des valeurs ordonnables (nombres entiers ou réels, chaînes de caractères...) dans lequel on rajoute la condition suivante :

Quel que soit le nœud considéré :

* la valeur de chaque nœud de son sous-arbre gauche est inférieure ou égale à la sienne
* la valeur de chaque nœud de son sous-arbre droit est strictement supérieure à la sienne

*Remarque :* on peut trouver des définitions dans lesquelles les relations d'ordre diffèrent. En particulier si l'on n'utilise que des relations d'ordre strictes ($<$ et non $\le$), l'arbre binaire n'aura pas de doublons

![Arbre binaire de recherche](images/ABR.png){width=40%}

Le caractère *récursif* de cette définition assure que la valeur minimale à l'échelle de l'*ABR* est située dans le nœud le plus à gauche (pas obligatoirement le nœud "en bas à gauche"). De même la valeur maximale est située dans le nœud le plus à droite.

Comme son nom l'indique, les *ABR* sont construits pour la recherche. Considérons un *ABR* bien équilibré de taille $n$. Sa hauteur est de l'ordre de $\log{_2(n)}$. Afin de déterminer si une valeur est dans l'arbre :

* on teste l'égalité de cette valeur et de celle de la racine de l'arbre. S'il y a égalité on renvoie `Vrai`
* s'il n'y a pas égalité, on compare les deux valeurs :
    * si la valeur cherchée est inférieure, on reprend récursivement la recherche dans le sous-arbre gauche
    * si la valeur cherchée est supérieure, on reprend récursivement la recherche dans le sous-arbre droit

Si le sous-arbre en question n'existe pas (le nœud n'a pas d'enfant à gauche ou à droite), la valeur n'est pas dans l'arbre : on renvoie `Faux`

On le comprend, cette méthode de recherche permet de répondre à la question en un maximum de $\log{_2(n)}$ étapes. La complexité est identique à celle de la recherche dichotomique : $\mathcal{O}(\log{_2(n)})$.Pour une valeur de $n$ suffisamment grande, c'est beaucoup plus avantageux qu'une simple recherche linéaire de complexité $\mathcal{O}(n)$.

Pourquoi n'utilise-t'on pas cette méthode pour toutes nos recherches ? Car avant de chercher dans un *ABR* il faut le construire.

Imaginons que nous ayons un *ABR* bien équilibré de taille $n$ (et donc de hauteur de l'ordre de $\log{_2(n)}$). Comment insérer une valeur ? Il faut procéder de la même façon que pour la recherche et descendre le long de l'arbre en s'orientant au fil des comparaisons entre la valeur à insérer et la valeur du nœud en question. Cette méthode est elle aussi de complexité $\mathcal{O}(\log{_2(n)})$.

![Insertions dans un *ABR*](images/insertions_ABR.png){width=60%}

Si on la répète pour $n$ nœuds, on obtient une méthode de construction d'un *ABR* de taille $n$ qui coûte $\mathcal{O}(n\log{_2(n)})$. Cette complexité correspond à la meilleure complexité possible pour un tri par comparaison. Donc la recherche dans un *ABR*, si l'on inclut la construction de l'arbre, n'est pas plus avantageuse que la recherche dichotomique qui nécessite de trier le tableau au préalable (et donc une étape en $\mathcal{O}(n\log{_2(n)})$).

1. Le fichier `ABR.py` contient une classe `ABR` incomplète. Coder les méthodes manquantes (`inserer`, `contient`, `maximum` et `minimum`).

2. On fournit dans le dossier `/python` le texte de "Guerre et Paix" de L. Tolstoï (disponible sur le site du [Projet Gutenberg](http://www.gutenberg.org/)). On demande de charger ce texte, retirer la ponctuation (chercher du côté des *expressions régulières* et du module `re`) et de construire l'*ABR* de tous les mots distincts de ce long roman. Cherchez ensuite un mot au hasard. On pourra comparer le temps de recherche dans l'*ABR* à celui d'une recherche linéaire classique.

# Graphes

Un *graphe* est constitué de sommets et d'arêtes (*vertex* et *edges* en anglais). Cet objet intervient autant dans la distribution de l'eau dans une ville (les sommets sont les bâtiments et les arêtes les tuyaux) que dans la modélisation d'internet (les ordinateurs et les câbles/liaisons sans-fils).

![Graphe non orienté](images/grapheNonOriente.png){width=40%}

Dans le graphe de la figure 2, $A$, $B$, $\dots$ sont des sommets, $(A,B)$, $(B,C)$ sont des arêtes.

Deux sommets sont *adjacents*, ou *voisins*, si une arête les relie. $A$ et $B$ sont adjacents mais pas $A$ et $E$.

Le *degré* d'un sommet est le nombre d'arête y arrivant. Ainsi le degré de $A$ est $3$, celui de $D$ est $4$

Un graphe peut être **orienté** ou non :

* dans un graphe non orienté, l'arête $A \rightarrow B$ est identique à l'arête $B \rightarrow A$. Si $A$ connaît $B$ alors $B$ connaît $A$

* dans un graphe orienté, les arêtes peuvent différer : dans une ville, il peut exister un chemin allant de $A$ vers $B$ mais pas de $B$ vers $A$, il suffit d'avoir un sens interdit !

![Graphe orienté](images/grapheOriente.png){width=40%}

Un graphe peut être pondéré ou non. Dans un graphe pondéré, on associe un nombre, un *poids*, à chaque arête. Un graphe permettant de déterminer le trajet le plus court entre deux sommets contient des informations sur la longueur des arêtes. Cette longueur peut être en kilomètres (on aura le chemin le plus court en distance) ou en minutes (on aura le trajet le plus court en temps).

![Graphe pondéré](images/graphePondere.png){width=40%}

## Implémentation

On peut représenter informatiquement les graphes de deux façons classiques : 

* avec des listes d'adjacence
* avec une matrice d'adjacence

### Liste d'adjacence

Dans cette implémentation, on se contente d'indiquer quels sommets sont reliés à quels autres à l'aide de listes. On peut par exemple utiliser des dictionnaires python.

```python
# Représentation du graphe de la figure 15
# à l'aide d'une liste d'adjacence
graphe = {
    "A" : ["B", "C", "D"],
    "B" : ["A", "D", "E"],
    "C" : ["A", "D"],
    "D" : ["A", "B", "C", "E"],
    "E" : ["B", "D"]
}
```

Cette représentation facile à mettre en oeuvre est à privilégier lorsque le nombre d'arêtes est faible par rapport au nombre de sommets.

On peut grâce à cette implémentation facilement connaître le degré un sommet :

```python
degre_A = len(graphe["A"])
```

### Matrice d'adjacence

Dans cette représentation, on utilise une matrice (un tableau de nombres). Il faut au préalable numéroter les sommets : $A$ sera le premier et correspondra à la première ligne/colonne, $B$ sera le deuxième et correspondra à la deuxième ligne/colonne...

![Graphes et matrices d'adjacences](images/matriceAdjacence.jpg){width=50%}

Au sein de la matrice on place des valeurs selon l'existence d'une arête entre le sommet de la ligne et celui de la colonne :

* si la valeur est de $0$, il n'y a pas d'arête
* si la valeur est de $1$, il existe une arête

Si l'on doit stocker un graphe pondéré, il suffit de placer le poids d'une arête dans la matrice.

Par exemple pour le graphe de la figure 17 :

$$
\begin{pmatrix}
0 & 5 & 8 & 1 \\
5 & 0 & 2 & 3 \\
8 & 2 & 0 & 7 \\
1 & 3 & 7 & 0 \\
\end{pmatrix}
$$

En python, on utilisera une liste de liste :

```python
graphe = [
  [0, 5, 8, 1],
  [5, 0, 2, 3],
  [8, 2, 0, 7],
  [1, 3, 7, 0]
]
```

*Remarque :* dans le cas d'un graphe non orienté, la matrice d'adjacence est symétrique par rapport à la diagonale

1. Le fichier `Graphes.py` contient deux classes `Graph_as_list` et `Graph_as_matrix` incomplètes. Coder ces classes.

## Algorithmes classiques

### Parcours en largeur d'abord

On retrouve un algorithme proche de celui rencontré avec les arbres à cette différence qu'il faut ici garder trace des sommets déjà rencontrés afin de ne pas "tomber" dans un cycle.

```
ParcoursLargeur(Graphe G, Sommet depart) :
    f est une File vide
    Enfiler depart dans f
    Marquer depart comme visité
    Tant que f est non vide :
        Défiler le sommet s
        Afficher s
        Pour tous les voisins v de s dans G :
            Si v n'a pas été visité
              Enfiler v dans f
              Marquer v comme visité
```

![Graphe à parcourir](images/parcours.png){width=50%}

Dans le cas de la figure 6 et en partant de $A$ on obtient : $A \rightarrow B \rightarrow C \rightarrow E \rightarrow D \rightarrow F \rightarrow G$. $F$ n'est visité qu'à la suite de $B$ et pas après $E$ car il a déjà été marqué.

Les parcours en largeur d'abord interviennent dans :

* la recherche du chemin le plus court d'un graphe non pondéré
* la recherche d'un arbre couvrant de taille minimale pour un graphe non pondéré (arbre contenant tous les sommets du graphe)
* les réseaux peer-to-peer afin de déterminer les sommets voisins
* l'indexation des pages web à l'aide de *bots*
* construire la carte des connaissances de niveau $k$ d'une personne dans un réseau (on parcourt $k$ largeur à partir de la personne étudiée)
* la détection de cycle
* ...


### Parcours en profondeur d'abord

Pour le parcours en profondeur d'abord, on peut utiliser une fonction récursive :

```
ParcoursProfondeur(graphe G, sommet depart) :
    Marquer depart comme visité
    Afficher depart
    Pour tous les voisins v de depart :
        Si v n'a pas été visité :
            ParcoursProfondeur(G, v)
```

Dans le cas du graphe de la figure 6 et en partant de $A$ on obtient : $A \rightarrow B \rightarrow D \rightarrow F \rightarrow E \rightarrow C \rightarrow G$

Le parcours en profondeur d'abord intervient dans :

* la recherche du chemin le plus court d'un graphe non pondéré
* la recherche d'un arbre couvrant de taille minimale pour un graphe non pondéré (arbre contenant tous les sommets du graphe)
* la détection de cycle dans un graphe
* *tri topologique* des sommets d'un graphe. Ce tri permet de classer les sommets selon une hiérarchie (le premier ancêtre, son descendant...). Cela peut servir par exemple dans un tableur à déterminer quelle est la cellule à recalculer en priorité et dont dépendent les autres valeurs
* ...

### Détermination de chemin

L'une des utilisations les plus courantes des graphes est la détermination de trajet : pour simplifier, chaque coin de rue est un des sommets du graphe, les arêtes sont les rues. Le plus souvent on considère un graphe pondéré : à chaque rue on associe une longueur ou un temps de parcours.

L'algorithme le plus classique permettant de déterminer le chemin le plus court entre deux sommets est l'algorithme de [Dijkstra](https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra). Bien qu'**hors programme**, c'est un classique de l'algorithmique. Voici son pseudo-code :

```
Dijkstra : G (graphe), debut (sommet) → liste des prédécesseurs
	visites est une liste vide
	distances est un dictionnaire qui, à chaque sommet de G, associe la valeur + infini
	distances[depart] prend la valeur 0
	prédécesseurs est un dictionnaire qui, à chaque sommet de G, associe la valeur None
	Tant qu’il existe des sommets non visités :
		Choisir le sommet s non visité tel que distances[s] est minimal
		Ajouter s à visites
		Pour chaque voisin v de s :
			Si distances[v] > distances[s] + poids[s → v] :
				distances[v] prend la valeur distances[s] + poids[s → v]
				prédécesseur[v] prend la valeur s
	Retourner prédécesseurs 
```

L'idée derrière l'algorithme est de construire un arbre couvrant (tous les sommets du graphe font partie de cet arbre) respectant les conditions suivantes :

* la racine de l'arbre est le sommet de départ
* chaque sommet du graphe initial est situé dans l'arbre et le chemin reliant la racine à ce sommet minimal (le plus court)

![L'algorithme de Dijkstra transforme un graphe en un arbre](images/dijkstra_graph_to_tree.png){width=50%}

Lors du déroulement de l'algorithme on associe à chaque sommet visité deux données :

* son prédécesseur dans le chemin le plus court depuis le départ
* la longueur de ce chemin optimal

Il peut arriver qu'un sommet soit rencontré à plusieurs reprises lors du déroulement de l'algorithme. Lorsque c'est le cas on vérifie que le chemin (le couple `(prédécesseur, distance)`) associé à ce sommet est toujours meilleur que le nouveau chemin. Si ce n'est pas le cas, on met à jour les informations du sommet.

L'algorithme de Dijkstra seul ne fournit pas le meilleur chemin entre deux sommets. En réalité, il fournit le meilleur chemin entre le **départ** et **tous** les autres sommets du graphe. Afin de déterminer le meilleur chemin entre le départ et un sommet quelconque, il faut utiliser le dictionnaire des prédécesseurs : si l'on souhaite arriver au sommet `Z`, on regarde son prédécesseur (l'algorithme nous assure qu'il est sur le chemin le plus court). On étudie ensuite le prédécesseur de ce nouveau sommet et l'on répète l'opération jusqu'à ce que l prédécesseur soit le sommet de départ.

La longueur du chemin le plus court s'obtient directement en récupérant la `distance` associée au sommet visé.

Afin de finir cette présentation, jouons aux "Aventuriers du Rail - Edition Bretagne !"

![La carte](images/bretagne.jpg){width=90%}

Le fichier `bretagne.py` contient le graphe associé au plateau de jeu. On utilise une implémentation à l'aide de matrice d'adjacence (afin de pouvoir donner des poids aux arêtes).

1. Compléter le fichier `plus_court_chemin.py` en codant les fonctions `dijkstra` et `trajet` permettant de déterminer le meilleur trajet entre deux sommets bretons.