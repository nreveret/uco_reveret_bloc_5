# UCO_reveret_Bloc_5

Vous trouverez dans ce dépôt les ressources concernant le bloc 5 du DIU "Enseigner la spécialité NSI" de l'UCO-Angers.

Ce dépôt comporte plusieurs dossiers correspondant aux différentes parties abordées dans ce bloc.

Dans chaque dossier vous trouverez :

* un fichier *markdown* : le fichier source de ce chapitre éditable et visualisable avec Visual Studio Code (par exemple)
* un fichier *pdf* : export du cours en *pdf* pour faciliter la lecture
* un fichier *html* : export du cours en *html* pour faciliter la lecture
* un dossier `python`. On y trouvera plusieurs fichiers python portant le même nom : `fichier.py` et `fichier_complet.py`. Le premier contient la base du code à taper, le second la correction
* des fichiers et dossiers annexes (images, documents complémentaires...)

Ce bloc fait appel à plusieurs reprises à de la *Programmation Orientée Objet* (qui est aussi au programme de Terminale NSI). Voici deux liens permettant de découvrir les bases de la *POO* en python :

* un [cours](https://python.doctor/page-apprendre-programmation-orientee-objet-poo-classes-python-cours-debutants)
* une [vidéo](https://www.youtube.com/watch?v=dfUM_9xibf8)