from time import perf_counter

def fibonacci(n: int) -> int:
    """
    Calcul le n-ième terme de la suite de Fibonacci
    Récursivité non-terminale
    """
    if n <= 1:
        return n
    else:
        return fibonacci(n-1) + fibonacci(n-2)


def fibonacci_TR(n: int, accu_1: int = 1, accu_2: int = 0) -> int:
    """
    Calcul le n-ième terme de la suite de Fibonacci
    Récursivité terminale
    """
    if n == 0:
        return accu_2
    elif n == 1 :
        return accu_1
    else:
        return fibonacci_TR(n-1, accu_1 + accu_2, accu_1)

if __name__ == "__main__" :
    n = 30

    t1 = perf_counter()
    print(f"Avec une récursion non terminale : F_{n} = {fibonacci(n)}")
    t2 = perf_counter()
    print(f"Calcul fait en {t2-t1} ms")
    t1 = perf_counter()
    print(f"Avec une récursion terminale :     F_{n} = {fibonacci_TR(n)}")
    t2 = perf_counter()
    print(f"Calcul fait en {t2-t1} ms")

