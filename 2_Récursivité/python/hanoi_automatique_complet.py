import os
import time
"""
Implémentation des tours de Hanoi dans la console
Le programme demande le nombre de cylindre
La solution sous la forme de "abbc" est ensuite tapée par l'utilisateur dans la console
Le programme déroule alors la solution dans la console
"""


def dessiner(a, b, c):
    """
    Dessine les piles
    """
    motif = "\n"

    liste_a = a[::]
    while len(liste_a) < taille:
        liste_a.append(0)
    liste_b = b[::]
    while len(liste_b) < taille:
        liste_b.append(0)
    liste_c = c[::]
    while len(liste_c) < taille:
        liste_c.append(0)

    for i in range(-1, -(taille+1), -1):
        motif += " " * (taille - liste_a[i]) + "X" * 2 * \
            liste_a[i] + " " * (taille - liste_a[i]) + " " * 2
        motif += " " * (taille - liste_b[i]) + "X" * 2 * \
            liste_b[i] + " " * (taille - liste_b[i]) + " " * 2
        motif += " " * (taille - liste_c[i]) + "X" * 2 * \
            liste_c[i] + " " * (taille - liste_c[i]) + "\n"

    motif += " " * (taille-1) + "a" + " " * taille + " " * 2
    motif += " " * taille + "b" + " " * taille + " " * 2
    motif += " " * taille + "c" + " " * taille

    print(motif)


if __name__ == "__main__":
    taille = int(input("Combien de piles souhaitez-vous ?"))
    solution = input("Tapez/Collez ici votre solution : ")
    instructions = [solution[i:i+2] for i in range(0, len(solution), 2)]

    a = []
    b = []
    c = []

    piles = {'a': a, 'b': b, 'c': c}

    for i in range(taille, 0, -1):
        a.append(i)

    for instruction in instructions:
        os.system('cls')
        time.sleep(0.1)  # Durée d'affichage (en seconde)

        pile_depart = piles[instruction[0]]
        pile_arrivee = piles[instruction[1]]

        if pile_depart:
            valeur_depart = pile_depart.pop()
            valeur_arrivee = taille+1
            if pile_arrivee:
                valeur_arrivee = pile_arrivee.pop()
            if valeur_arrivee > valeur_depart:
                if valeur_arrivee != taille+1:
                    pile_arrivee.append(valeur_arrivee)
                pile_arrivee.append(valeur_depart)
            else:
                pile_arrivee.append(valeur_arrivee)
                pile_depart.append(valeur_depart)
                print("Coup illégal !!")
                break
        else:
            print("La pile de départ est vide")
            break

        dessiner(a, b, c)
        print("Vous avez joué : ", instruction)

    input("Pressez 'Entrée' pour quitter")
