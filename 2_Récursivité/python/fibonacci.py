from time import perf_counter

def fibonacci(??) -> ??:
    """
    Calcul le n-ième terme de la suite de Fibonacci
    Récursivité non-terminale
    """
    pass


def fibonacci_TR(??) -> ??:
    """
    Calcul le n-ième terme de la suite de Fibonacci
    Récursivité terminale
    """
    pass

if __name__ == "__main__" :
    n = 5

    print(f"Avec une récursion non terminale : F_{n} = {fibonacci(n)}")
    print(f"Avec une récursion terminale :     F_{n} = {fibonacci_TR(n)}")

