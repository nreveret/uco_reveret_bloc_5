# Import du module Python Image Library
from PIL import Image


def mosaique(niveau: int, adresse_image: str) -> Image:
    """
    Création d'une mosaïque à partir de l'image indiquée en adresse

    niveau : entier. Le nombre de degrés de réductions
    adresse_image : l'adresse du fichier d'origine sur le disque
    """
    def reduction(img, niveau: int, x: int, y: int):
        """
        Réduit la taille de l'image de 2 et la dessine sur new_img
        lorsque l'on est arrivé en fin de récursion
        """
        if niveau == 1:
            l, h = img.size
            img = img.resize((l//2, h//2))
            new_img.paste(img, (x+0, y+0))
            new_img.paste(img, (x+l//2, y+0))
            new_img.paste(img, (x+0, y+h//2))
            new_img.paste(img, (x+l//2, y+h//2))
        else:
            l, h = img.size
            img = img.resize((l//2, h//2))
            reduction(img, niveau-1, x, y)
            reduction(img, niveau-1, x+l//2, y)
            reduction(img, niveau-1, x, y+h//2)
            reduction(img, niveau-1, x+l//2, y+h//2)

    # Chargement de l'image à modifier
    img = Image.open(adresse_image)

    # Récupération de ses dimensions
    l, h = img.size

    # Création d'une nouvelle image aux mêmes dimensions
    new_img = Image.new(mode="RGB", size=(l, h))

    # Processus de réduction et dessin sur la nouvelle image
    reduction(img, niveau, 0, 0)

    return new_img

if __name__ == "__main__":
    mosaique(3, "vache.jpg").show()

    