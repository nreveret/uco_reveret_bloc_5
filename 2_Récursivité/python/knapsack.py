from numpy import matrix
from random import randint

def sac_aleatoire(nb_objets : int, poids_max_objet : int, valeur_max_objet : int) -> tuple :
    poids = [randint(1,poids_max_objet) for _ in range(nb_objets)]
    valeurs = [randint(1,valeur_max_objet) for _ in range(nb_objets)]
    poids[0] = -1
    valeurs[0] = -1
    return poids, valeurs


def knapsack_ITER(N: int, W: int) -> list:
    """
    Résout le problème du sac à dos à l'aide de la programmation dynamique
    Version bottom-up
    """
    pass

def knapsack_RECUR_MEMO(N : int, W: int) -> list:
    """
    Résout le problème du sac à dos à l'aide de la programmation dynamique
    Version Top-down avec memoïsation
    """
    pass


def knapsack_RECUR(N : int, W: int) -> list:
    """
    Résout le problème du sac à dos à l'aide de la programmation dynamique
    Version Top-down sans memoïsation
    """
    pass


def solution(T : list) -> tuple :
    """
    Retourne la solution du problème du sac à dos
    """
    pass

if __name__ == "__main__":
    
    # Le problème du cours
    poids = [-1, 1, 3, 3, 4, 5]
    valeurs = [-1, 2, 1, 3, 5, 3]
    N = len(poids)-1
    W = 10

    """
    # Un problème quelconque
    N = 15
    p_max = 10
    v_max = 50 
    poids, valeurs = sac_aleatoire(N+1, p_max, v_max)
    W = N*p_max//3
    """

    print("*****************************************\nLE PROBLÈME")
    print(f"Le poids maximal du sac : {W}")
    print(f"Le nombre d'objets : {N}")
    print(f"Les poids des objets :   {poids[1:]}")
    print(f"Les valeurs des objets : {valeurs[1:]}")