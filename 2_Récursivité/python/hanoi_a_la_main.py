import os
"""
Implémentation des tours de Hanoi dans la console
"""

import re

def dessiner(a, b, c):
    """
    Dessine les piles
    """
    motif = "\n"

    liste_a = a[::]
    while len(liste_a) < taille :
        liste_a.append(0)
    liste_b = b[::]
    while len(liste_b) < taille :
        liste_b.append(0)
    liste_c = c[::]
    while len(liste_c) < taille :
        liste_c.append(0)
    
    for i in range(-1, -(taille+1), -1):
        motif += " " * (taille - liste_a[i]) + "X" * 2 * \
            liste_a[i] + " " * (taille - liste_a[i]) + " " * 2
        motif += " " * (taille - liste_b[i]) + "X" * 2 * \
            liste_b[i] + " " * (taille - liste_b[i]) + " " * 2
        motif += " " * (taille - liste_c[i]) + "X" * 2 * \
            liste_c[i] + " " * (taille - liste_c[i]) + "\n"

    motif += " " * (taille-1) + "a" + " " * taille + " " * 2
    motif += " " * taille + "b" + " " * taille + " " * 2
    motif += " " * taille + "c" + " " * taille

    print(motif)


if __name__ == "__main__":
    taille = int(input("Combien de piles souhaitez-vous ?"))

    a = []
    b = []
    c = []

    piles = {'a' : a, 'b': b, 'c' : c}

    for i in range(taille, 0, -1):
        a.append(i)

    while True:
        os.system('cls')
        dessiner(a,b,c)
        instruction = input("Quel coup voulez-vous jouer (ab, ba ...) ? (Tapez 'fin' pour quitter) ")
        if instruction == "fin" :
            break
        while not re.search("[abc]{1}[abc]{1}", instruction):
            instruction = input(
                "Saisir l'instruction sous la forme colonne-colonne. Par exemple ab ou ba : ")

        pile_depart = piles[instruction[0]]
        pile_arrivee = piles[instruction[-1]]

        if pile_depart:
            valeur_depart = pile_depart.pop()
            valeur_arrivee = taille+1
            if pile_arrivee:
                valeur_arrivee = pile_arrivee.pop()
            if valeur_arrivee > valeur_depart:
                if valeur_arrivee != taille+1:
                    pile_arrivee.append(valeur_arrivee)
                pile_arrivee.append(valeur_depart)
            else:
                pile_arrivee.append(valeur_arrivee)
                pile_depart.append(valeur_depart)
                print("Coup illégal !!")
                input("Pressez 'Entrée' pour jouer un autre coup")
        else :
            print("La pile de départ est vide")
            input("Pressez 'Entrée' pour jouer un autre coup")
