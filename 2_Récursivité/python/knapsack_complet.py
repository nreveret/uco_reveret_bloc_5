from numpy import matrix
from random import randint

def sac_aleatoire(nb_objets : int, poids_max_objet : int, valeur_max_objet : int) -> tuple :
    poids = [randint(1,poids_max_objet) for _ in range(nb_objets)]
    valeurs = [randint(1,valeur_max_objet) for _ in range(nb_objets)]
    poids[0] = -1
    valeurs[0] = -1
    return poids, valeurs


def knapsack_ITER(N: int, W: int) -> list:
    """
    Résout le problème du sac à dos à l'aide de la programmation dynamique
    Version bottom-up
    """
    global compteur

    T = []
    for w in range(W+1):
        l = []
        for n in range(N+1):
            l.append(0)
        T.append(l)

    for i in range(1, N+1):
        for c in range(0, W+1):
            compteur += 1
            if c - poids[i] >= 0:
                T[c][i] = max(T[c-poids[i]][i-1] + valeurs[i], T[c][i-1])
            else:
                T[c][i] = T[c][i-1]

    return T

def knapsack_RECUR_MEMO(N : int, W: int) -> list:
    """
    Résout le problème du sac à dos à l'aide de la programmation dynamique
    Version Top-down avec memoïsation
    """
    global compteur
    compteur += 1

    
    if N == 0 or W == 0 : # Cas de bases
        return 0
    else : # Cas généraux
        if W - poids[N] >= 0 : # La valeur est-elle calculable ?
            if T[W-poids[N]][N-1] == 0 : # n'a-t-elle pas encore été calculée ?
                T[W-poids[N]][N-1] = knapsack_RECUR_MEMO(N-1, W-poids[N])
        if T[W][N-1] == 0 : # cette valeur n'a-t-elle pas encore été calculée ?
            T[W][N-1] = knapsack_RECUR_MEMO(N-1, W)
        
        if W - poids[N] >= 0 : # On est "au milieu" du tableau, on calcule le max
            T[W][N] = max(T[W-poids[N]][N-1]+valeurs[N], T[W][N-1])
        else : # On ne peut pas remonter les lignes, on copie la valeur de gauche
            T[W][N] = T[W][N-1]
        return T[W][N]


def knapsack_RECUR(N : int, W: int) -> list:
    """
    Résout le problème du sac à dos à l'aide de la programmation dynamique
    Version Top-down sans memoïsation
    """
    global compteur
    compteur += 1

    if N == 0 or W == 0 : # Cas de bases
        return 0
    else : # Cas généraux
        if W - poids[N] >= 0 : # La valeur est-elle calculable ?
            T[W-poids[N]][N-1] = knapsack_RECUR(N-1, W-poids[N])
        T[W][N-1] = knapsack_RECUR(N-1, W)
        
        if W - poids[N] >= 0 : # On est "au milieu" du tableau, on calcule le max
            T[W][N] = max(T[W-poids[N]][N-1]+valeurs[N], T[W][N-1])
        else : # On ne peut pas remonter les lignes, on copie la valeur de gauche
            T[W][N] = T[W][N-1]
        return T[W][N]


def solution(T : list) -> tuple :
    """
    Retourne la solution du problème du sac à dos
    """
    W =  len(T)-1
    N = len(T[0])-1

    i,j = W,N

    s = []

    while j > 0 :
        if T[i][j] == T[i][j-1] :
            s.append(0)
            i,j = i,j-1
        else :
            s.append(1)
            i,j = i-poids[j], j-1
    
    s.reverse()

    return s



if __name__ == "__main__":
    """
    # Le problème traité dans le cours
    poids = [-1, 1, 3, 3, 4, 5]
    valeurs = [-1, 2, 1, 3, 5, 3]
    N = len(poids)-1
    W = 10
    """
    
    # Un problème quelconque
    N = 15
    p_max = 10
    v_max = 50 
    poids, valeurs = sac_aleatoire(N+1, p_max, v_max)
    W = N*p_max//3
    

    print("*****************************************\nLE PROBLÈME")
    print(f"Le poids maximal du sac : {W}")
    print(f"Le nombre d'objets : {N}")
    print(f"Les poids des objets :   {poids[1:]}")
    print(f"Les valeurs des objets : {valeurs[1:]}")


    # Version itérative
    # Une variable globale pour compter les cases du tableau remplies
    compteur = 0
    T = knapsack_ITER(N, W)
    print("\n*****************************************\nITÉRATIF")
    print(matrix(T))
    s_ITER = solution(T)
    print(s_ITER)
    print(f"{compteur} cases remplies")
    

    print("\n*****************************************\nRECURSIF SANS MEMOISATION")
    # Version récursive sans memoisation
    # => On crée le tableau T
    T = []
    for w in range(W+1):
        l = []
        for n in range(N+1):
            l.append(0)
        T.append(l)
    # Une variable compteur pour compter les appels récursifs
    compteur = 0
    knapsack_RECUR(N, W)
    print(matrix(T))
    s_RECUR = solution(T)
    print(s_RECUR)
    print(f"{compteur} appels récursifs")


    print("\n*****************************************\nRECURSIF AVEC MEMOISATION")
    # Version récursive avec memoisation
    # => On crée le tableau T
    T = []
    for w in range(W+1):
        l = []
        for n in range(N+1):
            l.append(0)
        T.append(l)
    # Une variable compteur pour compter les appels récursifs
    compteur = 0
    knapsack_RECUR_MEMO(N, W)
    print(matrix(T))
    s_MEMO = solution(T)
    print(s_MEMO)
    print(f"{compteur} appels récursifs")

    print("\n*****************************************\nCOMPARAISON DES SOLUTIONS")
    print(s_ITER == s_RECUR and s_RECUR == s_MEMO)
    