# Conversion de tous les fichiers MD du dossier en PDF ET HTML

import os
import subprocess

for file in os.listdir("."):
    if file.endswith(".md"):
        nom_fichier = os.path.join(os.path.splitext(file)[0])
        md = nom_fichier + ".md"
        print(md)
        pdf = nom_fichier + ".pdf"
        html = nom_fichier + ".html"
        subprocess.run(
            f"pandoc -s --pdf-engine=pdflatex --highlight-style tango -V colorlinks -V urlcolor=NavyBlue {md} -o {pdf}")
        subprocess.run(
            f"pandoc -s -c pandoc_nico.css -s --mathjax --highlight-style tango -V colorlinks -V urlcolor=NavyBlue {md} -o {html}")
