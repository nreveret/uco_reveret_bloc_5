---
title: |
    DIU Enseigner la spécialité NSI \
    Bloc 5 \
    - Récursivité, Diviser pour régner, Programmation dynamique -

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhf{}
    \lhead{DIU Enseigner la spécialité NSI}
    \rhead{Récursivité, Diviser pour régner, Programmation dynamique}
    \cfoot{\thepage}

author: |
    N. Revéret

lang:
- french

geometry:
- top=2.5cm
- bottom=2cm
- left=2cm
- right=2cm

output:
  pdf_document:
    fig_caption: yes
    table_caption: no
    df_print: kable
  html_document:
    fig_caption: yes
    df_print: paged
---

La récursivité est une notion à l'intersection des mathématiques et de l'informatique. Très efficace en terme de concision du code, c'est aussi une méthode très simple pour rendre un code défectueux.

# Récursivité

## Présentation

Une fonction récursive est une fonction qui lors de son exécution s'appelle elle-même.

![La récursivité](images/recursion.png){width=40%}

L'exemple archi-classique est celui de la fonction calculant la *factorielle* d'un nombre. On rappelle que la factorielle d'un entier naturel $n \ge 0$ vaut $1$ si $n$ vaut $0$ (par convention) ou :

$$factorielle(n) = n\,! = n \times (n-1) \times \dots \times 1$$

Un code récursif possible pour une telle fonction est :

```python
def facto(n : int) -> int :
    if n <= 1 : # Cas de base :
    return 1
  else : # Cas général
    return n * facto(n - 1)
```

Comme on peut le voir, le code est structuré autour de deux cas :

* le **cas de base** : permet de sortir de la récursivité, sans lui, on rentrerait dans une boucle infinie (ou en tout cas une boucle qui ferait *bugger* python)
* le **cas général** : c'est dans ce bloc que s'effectue l'appel récursif. Notez l'appel de la même fonction avec une valeur décrémentée du paramètre (`n - 1`)

Que se passe-t'il lorsque l'on appelle `facto(4)` ? Python exécute le code ainsi :

$$\begin{aligned}
  facto(4) &= 4\times facto(3) \\ 
  &= 4\times 3 \times facto(2) \\ 
  &= 4 \times 3 \times 2 \times facto(1) \\
  &= 4 \times3 \times 2 \times 1 \\
  &= 24
\end{aligned}
$$

Pour bien illustrer ce cas de figure, considérons la fonction suivante :

```python
def inc(n: int) -> None:
    if n == 5:
        print(f"On s'arrête à n={n}")
    else:
        print(f"n vaut {n}")
        return inc(n+1)
```

Que produit l'appel `inc(0)` ?

```
n vaut 0
n vaut 1
n vaut 2
n vaut 3
n vaut 4
On s'arrête à n=5
```

Que se passe-t'il ? En fait la fonction s'appelle elle-même en incrémentant la valeur du paramètre de $1$ à chaque fois. Lorsque `n` vaut $5$, les appels récursifs se terminent.

Une bonne façon de décrire le fonctionnement de ces appels est un *pile* : lors des appels successifs, le système les empile sur la **pile d'exécution**. A chaque nouvel empilement, l'exécution d'une fonction est mise en attente jusqu'à ce qu'elle soit de nouveau en haut de la pile.

Lorsqu'une fonction se termine (avec un `return` par exemple), elle est dépilée et son résultat est transmis à la fonction qui l'avait appelée, fonction qui était juste en-dessous dans la pile et qui se retrouve désormais au sommet. Cette dernière est alors exécutée....

On peut illustrer cela avec la figure 2.

![La pile d'exécution de `inc(0)`](images/pileExecution2.png){width=90%}

*Remarque :* Que se passe-t'il si l'on appelle `inc(6)` ?

```
n vaut 6
n vaut 7 
n vaut 8 
n vaut 9 
...
RecursionError: maximum recursion depth exceeded while calling a Python object
```

On obtient une erreur. En effet, notre fonction incrémente `n` jusqu'à ce qu'il soit égal à $5$. Or en débutant à $6$, on n'atteindra jamais la valeur $5$ : le système empile donc des fonctions dans la pile d'exécution sans jamais dépiler : la pile déborde ([stack overflow !](https://stackoverflow.com/)). Une sécurité du langage permet de limiter ces cas de figure aux alentours de $1\,000$ appels récursifs (cette valeur dépend de l'OS).

Il est possible de déterminer la profondeur limite de récursivité utilisé par python en utilisant [`sys.getrecursionlimit()`](https://docs.python.org/3/library/sys.html#sys.getrecursionlimit). On peut la modifier en faisant [`sys.setrecursionlimit(nouvelle_limite)`](https://docs.python.org/3/library/sys.html#sys.setrecursionlimit).

## Récursivité terminale

Le code présenté ci-dessus pour le calcul de la factorielle d'un entier n'est pas idéal. Pour bien s'en rendre compte reprenons la suite de calculs effectués lors de l'appel de `facto(4)` :

$$\begin{aligned}
  facto(4) &= 4\times facto(3) \\ 
  &= 4\times 3 \times facto(2) \\ 
  &= 4 \times 3 \times 2 \times facto(1) \\
  &= 4 \times3 \times 2 \times 1 \\
  &= 24
\end{aligned}
$$

Comme on peut le voir, le calcul à effectuer "gonfle" à chaque étape. Imaginez le calcul de `facto(1000)`... Ce point pose problème car il va engendrer une grande occupation de la mémoire pour python :

* d'une part lors de l'exécution, la pile d'exécution va se remplir avec l'ensemble des appels (classique lors de la récursivité, on ne peut pas vraiment faire autrement...)
* d'autre part, une zone de mémoire va devoir stocker les valeurs précédant chacun des $facto(n)$ dans les calculs lors de chaque appel (stockage de $4$, stockage de $3$, stockage de $2$...)

La raison fondamentale de ce problème est que la récursivité n'est pas *terminale* : l'appel récursif n'est pas le dernier à être exécuté par le code. En effet dans la ligne `return n * factorielle(n - 1)`, la dernière opération (en termes de priorités opératoires) est la multiplication. A cause de cela, python a besoin de stocker les valeurs intermédiaires à chaque appel pour effectuer la multiplication une fois que l'appel a retourné une valeur.

Une façon de transformer ce code en **récursivité terminale** (*tail recursion*) est d'utiliser un accumulateur :

```python
def facto_TR(n : int, accu = 1 : int) -> int :
    if n <= 1 : # Cas de base :
    return accu
  else : # Cas général
    return facto_TR_(n - 1, n * accu)
```

Observons les calculs effectués pour voir la différence :

$$\begin{aligned}
  facto(4) &= facto(3, 4 \times 1) \\ 
  &= facto(2, 3 \times 4) \\ 
  &= facto(1, 2 \times 12) \\ 
  &= 24
\end{aligned}
$$

Le calcul ne "gonfle" pas au fur et à mesure, la mémoire est "soulagée".

L'un des autres avantages de cette façon de formuler les fonctions récursives et de pouvoir facilement les transformer en fonctions *itératives*, en boucle `Pour` par exemple :

```python
def facto_ITER(n : int, accu = 1 : int) -> int :
  accu = 1
  for i in range(1, n+1) :
    accu *= i
  return accu
```

On retrouve bien notre accumulateur.

*Remarques :* 

* certains langages de programmation (Scala par exemple) peuvent optimiser les fonctions récursives terminale lors de la compilation en les transformant en fonctions itératives. Ce n'est pas le cas de python
* dans certains langages (Haskell, Javascript...), lorsque le compilateur "comprend" que l'appel récursif à effectuer est terminal, la fonction appelante est retirée de la pile d'exécution et remplacée par le nouvel appel. Voir cet article de [wikipedia](https://en.wikipedia.org/wiki/Tail_call)

## Applications

1. La suite de *Fibonacci* est décrite par les relations suivantes :

$$F_0 = 0$$
$$F_1 = 1$$
$$\textbf{Pour tout entier naturel } n \ge 2:\;F_{n} = F_{n-1}+ F_{n-2}$$

Coder la fonction calculant le $n$-ième terme de cette suite à l'aide d'une fonction récursive classique puis d'une fonction récursive terminale.

2. Chronométrez l'exécution des deux codes précédents pour le calcul de $F_{30}$. Qu'observe-t'on ?

2. Coder la fonction permettant de transformer une image en une mosaïque à l'image de la figure 3.

![Mosaïques de vaches qui rient](images/vaches.png){width=50%}

# Diviser pour régner

## Présentation

Considérons le problème suivant : nous sommes face à un grand tas de billes rouges, bleues et vertes et nous souhaitons les trier par couleur. L'une des possibilités est bien sûr de piocher les billes au fur et à mesure et de les placer dans le tas correspondant à sa couleur.

Une autre façon de faire consiste à :

* partager le tas en deux autant de fois que nécessaire jusqu'à obtenir des petits tas faciles à manipuler,
* trier les billes de chacun de ces sous-tas,
* regrouper les petits tas de chaque couleur pour obtenir la solution finale

Cette méthode est appelée *Diviser Pour Régner*. L'expression est attribuée à Philippe II de Macédoine, le père d'Alexandre le Grand. Il fallait alors l'entendre dans un sens politique.

La méthode diviser pour régner (*divide and conquer* en anglais) consiste en trois étapes :

1. Partager le problème en sous-problèmes de taille inférieure (*diviser*)
2. Résoudre les sous-problèmes (*régner*)
3. Combiner leurs solutions afin de résoudre le problème de départ (*combiner*)

![Diviser pour régner](images/divide_conquer.png){width=40%}

L'intérêt de cette méthode est double :

* dans certains cas, un problème se résout plus facilement à l'aide de ses sous-problèmes que frontalement (dans le jeu de la tour de Hanoï, avant de déplacer $5$ anneaux, il faut en déplacer $4$...)
* le partage en sous-problèmes et leur résolution peut aussi faire baisser la complexité de l'algorithme (le tri fusion est de complexité $O(n \log{_2(n)}))$

Concernant la complexité, le *[théorème général](https://fr.wikipedia.org/wiki/Master_theorem)* (clairement hors programme pour nos lycéens) permet d'obtenir des résultats. On en donne ci-dessous une explication très simplifiée (sans parler de la preuve !).

Le théorème considère le cas où, afin de résoudre un problème de taille $n$, on résout $a$ sous-problèmes de taille $\frac{n}{b}$. A chaque étape de la résolution, des calculs liés aux phases *diviser* et *combiner* sont nécessaires. Leur nombre dépend de la taille du problème et se calcule à l'aide d'une fonction $f$.

On peut alors considérer que le nombre de calculs nécessaires pour résoudre le problème de taille $n$ vaut :

$$T(n)=aT(\frac{n}{b}) + f(n)$$

(on résout $a$ sous-problèmes de taille $\frac{n}{b}$ et les divisions et combinaisons des solutions "coûtent" $f(n)$)

La complexité de la méthode dépendra alors de la comparaison de $f(n)$ avec $n^{\log{_b(a)}}$ : 

* si $f(n)$ est "majorée" (le théorème est plus précis mais passons) par une fonction polynôme de degré (puissance maximale) **strictement inférieure** à $\log{_b(a)}$ alors la complexité est de classe $\Theta (n^{\log{_b(a)}})$ (cas très intéressant)

* si $f(n)$ est majorée par une fonction polynôme de degré **égal** à $\log{_b(a)}$ alors la complexité est de classe $\Theta (n^{\log{_b(a)}} \log{_b(n)})$ (cas intéressant)

* si $f(n)$ est majorée par une fonction polynôme de degré **strictement supérieur** à $\log{_b(a)}$ alors la complexité est de classe $\Theta (f(n))$ (cas non intéressant)

Donnons quelques exemple afin d'éclaircir tout cela :

* On partage le problème en $3$ sous-problèmes de taille $\frac{n}{2}$ et le coût des opérations de divisions et de combinaisons est linéaire. On a donc $a=3$, $b=2$ et $f(n)=O(n^1)$. Or $\log{_2(3)}\approx 1,58 > 1$. Les calculs résiduels coûtent en quelque sorte moins que le partage et la situation est avantageuse (premier cas ci-dessus)

* En reprenant les mêmes valeurs de $a$ et $b$, si $f(n)=O(n^2)$ alors le problème n'est pas avantageux car les calculs résiduels coûtent trop cher ($\log{_2(3)}\approx 1,58 < 2)$. C'est le dernier cas ci-dessus

* Enfin si l'on partage le problème en $\bold{2}$ sous-problèmes de taille $\frac{n}{2}$ et que le coût des opérations de divisions combinaisons est encore linéaire on a $a=b=2$ et $f(n)=O(n^1)$. Or $\log{_2(2)}=1$ : on est dans le cas médian et notre algorithme est de complexité $\Theta (n^{\log{_2(2)}} \log{_2(n)}) = \Theta (n \log{_2(n)})$. Ce cas de figure est celui du *tri fusion*

## Applications

### Multiplications d'entiers

Si l'on souhaite multiplier deux nombres entiers positifs à $8$ chiffres en base 10, il est possible de procéder ainsi :

$$\begin{aligned}
  12\,345\,678 \times 87\,654\,321 &= (1\,234 \times 10^4 + 5\,678) \times (8\,765 \times 10^4 + 4\,321) \\ 
  &= 1\,234 \times 8\,765 \times 10^{8} + 1\,234 \times 4\,321 \times 10^{4} + 5\,678 \times 8\,765 \times 10^{4} + 5\,678 \times 4\,321
  \end{aligned}
$$

On a ainsi transformé un problème de taille $8$ (taille d'un des nombres) en $4$ problèmes de taille $4=\frac{8}{2}$. Le coût des opérations de combinaisons des résultats des sous-multiplications est lié à la taille des nombres (il s'agit d'additions) : sa complexité est linéaire.

On peut alors recommencer cette opération en transformant nos multiplications de taille $4$ en $4$ sous-multiplications de taille $2$... Voici une application de *diviser pour régner* !

De façon générale on a donc :

$$(a\times 10^{k}+b)(c\times 10^{k}+d)=ac\times 10^{2k}+(ad+bc)\times 10^{k}+bd$$

1. Quelle est la relation de récurrence lie le coût d'une multiplication de nombres de taille $n$ à celle de nombres plus petits ?

2. Appliquer le *master theorem* afin de déterminer la complexité de cette méthode.

En 1960 [Anatoly Karatsuba](https://en.wikipedia.org/wiki/Anatoly_Karatsuba), alors âgé de 23 ans, propose la méthode suivante :

$$(a\times 10^{k}+b)(c\times 10^{k}+d)=ac\times 10^{2k}+(ac+bd-(a-b)(c-d))\times 10^{k}+bd$$

Son idée est "juste" de repérer que $ad+bc=ac+bd-(a-b)(c-d)$...

Toujours est-il qu'il arrive donc à transformer une problème de taille $n$ en $3$ sous-problèmes de taille $\frac{n}{2}$ ! En effet son calcul ne fait intervenir  que $3$ sous-produits $ac$, $bd$ et $(a-b)(c-d)$, ce dernier calcul s'effectuant à partir de résultats de soustractions (obtenus en complexité linéaire). Le coût des calculs de recombinaison n'est pas modifié et est toujours en $\mathcal{O}(n)$.

3. Quelle est la complexité de cette méthode ?

### La tour de Hanoï

Le but du jeu de la tour de Hanoï est de déplacer les anneaux d'une tige `a` vers une tige `c` en respectant les contraintes suivantes :

* à chaque tour on ne peut déplacer qu'un seul anneau
* il est interdit d'empiler un anneau sur un anneau plus petit (les anneaux doivent toujours être classés sur les tiges)

Le fichier `hanoi_a_la_main.py` vous permet de tester ce jeu dans la console.

Après quelques essais on comprend que la résolution du problème consistant à déplacer $5$ anneaux de la tige `a` vers la `c` nécessite :

* de déplacer les $4$ premiers de la tige `a` vers la `b`
* déplacer l'anneau le plus grand de la tige `a` vers la `c`
* de déplacer les $4$ anneaux de la tige `b` vers la `c`

On considère la fonction `solution` qui prend en argument le nombre d'anneaux à déplacer, la tige de départ, la tige d'arrivée ainsi que la tige de stockage temporaire et renvoie la succession des déplacements à faire. Par exemple `solution(2, 'a','c', 'b')` renvoie `'abacbc`. On interprète ce résultat comme la succession des déplacements `a` $\rightarrow$ `b` puis `a` $\rightarrow$ `c` et enfin `b` $\rightarrow$ `c`. 

1. Exprimer `solution(n, 'a', 'c', 'b')` en fonction de `solution(n-1, ?, ?, ?)` (on remplacera les `?` par des valeurs adéquates).

2. Coder cette fonction. Le fichier `hanoi_automatique_complet.py` permet de vérifier vos solutions. Il suffit de copier/coller votre proposition dans la liste de déplacements à effectuer.

### Tri fusion

Le principe du tri fusion est de couper récursivement le tableau en deux jusqu'à obtenir des tableaux de taille $1$. Une fois ceux-ci atteints, on fusionne ces tableaux (qui sont de fait déjà triés) en piochant à chaque étape de plus petit élément de chaque tableau.

![Tri fusion](images/merge_sort.png){width=35%}

Cette méthode est construite autour de deux fonctions :

* `Tri_fusion` : partage le tableau en deux sous-tableaux. C'est la fonction récursive

* `Fusion` : fusionne les deux sous-tableaux passés en argument

C'est un tri stable mais pas en place (dans la version présentée ici).

```
Fonction Tri_Fusion(T : tableau) -> tableau :
    """
    Partage le tableau en deux sous-tableaux, trie chacun d'eux et renvoie leur fusion

    T est le tableau à trier

    Renvoie un nouveau tableau contenant les mêmes valeurs que T mais triées
    """
    # Cas de base
    Si longueur(T) == 1 :
        Renvoyer T
    Sinon : # Cas général
        # L'indice du "milieu" du tableau
        milieu = longueur(T)// 2 # division entière

        # Les sous-tableaux de gauche et de droite
        gauche = []
        droite = []
        # On copie la première moitié de T dans gauche,
        # La seconde dans droite
        Pour i allant de 0 à longueur(T) - 1 (inclus) :
            Si i < milieu :
                Ajouter T[i] à la fin de gauche
            Sinon :
                Ajouter T[i] à la fin de droite

        # Tri de gauche et droite
        gauche = fusion(gauche)
        droite = fusion(droite)

        # On renvoie leur fusion
        Renvoyer Fusion(gauche, droite)

Fonction Fusion(G : tableau, D : tableau) -> tableau :
    """
    Fusionne les deux tableaux proposés en un nouveau tableau
    A chaque étape on sélectionne le plus petit élément situé au 
    début de l'un des deux tableaux

    G et D sont les tableaux à fusionner

    Renvoie un nouveau tableau contenant les valeurs de G et D
    """

    R = [] # Le tableau qui sera renvoyé

    # On ajoute "infin" à la fin des tableaux pour simplifier la fusion
    Ajouter la valeur "infini" à la fin de G # en python on peut utiliser float('inf')
    Ajouter la valeur "infini" à la fin de D

    Tant que longueur(G) > 1 et longueur(D) > 1 : # les seuls éléments restants seront les infinis
        Si G[0] <= D[0] :
            Ajouter G[0] à la fin de R
            Retirer G[0] de G
        Sinon :
            Ajouter D[0] à la fin de R
            Retirer D[0] de D
    
    Renvoyer R
```

1. Coder le tri fusion.

# Programmation dynamique

## Présentation


Considérons de nouveau le problème du calcul du $n$-ième terme de la suite de Fibonacci. Un algorithme de résolution en python est :

```python
def fibonacci_recursif(n):
    if n <= 1:
        return n
    else:
        return fibonacci_recursif(n-1) + fibonacci_recursif(n-2)
```

Cette présente un **gros** problème. Mettons que l'on souhaite calculer $F_{10}$. Ce calcul aura besoin de $F_9$ et de $F_8$. Mais la valeur de $F_9$ nécessitera aussi $F_8$. Le programme proposé plus haut recalculera cette valeur sans se soucier du fait qu'elle ait déjà été déterminée... A vrai dire, le calcul de $F_{10}$ par cette méthode nécessite $177$ appels à la fonction `fibonacci_recursif` alors que $8$ calculs devraient suffire (ceux de $F_2$ à $F_{10}$) ! Le calcul de $F_{40}$ nécessite $331\,160\,281$ appels là où $38$ calculs devraient suffire...

![Le graphe des dépendances de $F_{10}$](images/intro.png){width=75%}

Comment faire mieux ? Il suffirait de garder la trace des termes de la suite déjà calculés. Mettons que l'on souhaite calculer $F_{n}$:

* s'il a déjà été calculé et sauvé dans un tableau `F`, on renvoie la valeur directement
* s'il n'a pas été déjà calculé :
  * si $n$ vaut $0$ ou $1$, on retourne $n$ (car $F_0=0$ et $F_1=1$)
  * sinon, on calcule la valeur de $F_n$ en appelant la fonction récursivement

On s'est donc prémunis des calculs redondants en s'assurant que la valeur est connue ou non. L'algorithme est le suivant :

```
F est un tableau de n+1 nombres
Pour tout i allant de 0 à n :
  F[i] = -1

fibonacci_dynamique(n) :
  Si F[n] vaut -1:
        Si n est inférieur ou égal à 1:
            F[n] prend la valeur n
        Sinon :
            F[n] prend la valeur fibonacci_dynamique(n-1) + fibonacci_dynamique(n-2)
  Renvoyer F[n]
```

Cette approche permet de calculer $F_{10}$ en ne faisant que $19$ appels (on rappelle qu'une valeur se calcule à partir des deux précédentes et $19 \approx 2 \times 10$).

Qu'avons nous fait ?

* On a découpé un problème compliqué en sous-problèmes
* On a calculé récursivement les solutions de ces sous-problèmes **en gardant la trace des résultats obtenus**
* On a combiné ces résultats afin de résoudre le problème principal

Cette approche est celle de la **programmation dynamique** développée par [Richard Bellman](https://en.wikipedia.org/wiki/Richard_Bellman) dans les années 1950 :

1. on considère un problème d'**optimisation** (la détermination de la meilleur solution à un problème)
2. on découpe ce problème en **sous-problèmes** dont on cherche la solution optimale. Il faut être sûre que la solution optimale au problème de départ fait intervenir la solution optimale d'un des sous-problèmes traités (*principe d'optimalité*)
3. les sous-problèmes sont **interdépendants** : on les résout en gardant trace des solutions intermédiaires afin d'éviter les répétitions
4. on **combine** ces solutions afin de résoudre le problème initial

Le procédé permettant de garder trace des calculs intermédiaires s'appelle la **mémoïsation** (il n'y a pas de "r", ce n'est pas une faute d'orthographe).

Le lien entre les solutions des sous-problèmes et celle du problème de taille supérieure fait intervenir une relation de récurrence. Dans le cas de la suite de Fibonacci, c'est la relation $F_n=F_{n-1}+F_{n-2}$. Cette relation traduit le *principe d'optimalité*. C'est ce principe qui justifie le découpage d'un problème de taille $n$ en sous-problèmes de tailles inférieures. Il affirme que si l'on connaît une solution optimale à notre problème alors toute partie de cette solution est aussi optimale.

Dans le cas d'une étude de trajet par exemple, si l'on connaît le plus court chemin entre deux sommets $A$ et $Z$ alors toute portion de ce chemin doit être optimale. En effet si l'on extrait un sous-chemin $C\rightarrow Y$ de notre trajet optimal et que l'on réalise qu'il existe un chemin plus court entre ces deux sommets alors il serait possible de relier $A$ et $Z$ par un trajet plus court ce qui est en contradiction avec le caractère optimal de notre trajet.

![Le chemin $C\rightarrow Y$ est aussi optimal](images/optimal.png){width=50%}

On a déterminé les valeurs de termes ici en débutant avec la valeur finale et en **descendant** jusqu'à la valeur $F_1$. On parle d'approche *Top-Down*. Il serait aussi possible de résoudre ce problème en partant des valeurs de base ($F_0$ et $F_1$) et en progressant de proche en proche jusqu'à obtenir la valeur cherchée $F_n$. On parle alors d'approche **ascendante** (ou *Bottom-Up*). Le choix de la "direction" à adopter dépend du problème :

* une approche descendante peut avoir tendance à recalculer certaines valeurs ce qui entraîne un surcoût
* une approche ascendante de par son côté itératif et exhaustif calculera quant à elle l'ensemble des cas intermédiaires. Or certains d'entre eux n'interviennent peut-être pas dans la résolution du problème visé...
  
![*Top-Down* vs *Bottom-Up*](images/top_down_bottom_up.png){width=50%}

## Application au problème du sac à dos

Le problème du sac à dos (*Knapsack* en anglais) est un classique de l'algorithmique. On considère :

* $N$ objets
* chaque objet $i$ ($1 \le i \le n$) a un poids $w_i$
* chaque objet $i$ ($1 \le i \le n$) a une valeur $v_i$
* un sac à dos de poids maximal $W$

Le problème est de placer des objets dans le sac à dos sans dépasser son poids maximal mais en maximisant la valeur totale des objets.

![Un sac à dos et des objets](images/Knapsack.png){width=40%}

Considérons le problème suivant :

|Objet|1|2|3|4|5|
|:-:|:-:|:-:|:-:|:-:|:-:|
|Poids|1|3|3|4|5|
|Valeur|2|1|3|5|3|

Avec les notations citées plus haut on a donc $N=5$, $w_1=1$ et $v_1=2$.

Le sac a un poids maximal de $W=10$.

On appelle "solution" du problème la donnée d'un quintuplet de *bits* $(s_i)_{1 \le i \le 5}$ , chacun indiquant si l'objet est pris ou non. Par exemple la solution $(1,1,0,1,0)$ signifie que l'on prend les objets $0$, $1$ et $3$. Une solution optimale est ici $(1,0,1,1,0)$ pour un poids total de $8$ et une valeur totale de $10$. On pourrait obtenir la même valeur totale en faisant $(1,0,0,1,1)$. La valeur d'une solution correspond à la somme suivante :

$$ \sum_{i=1}^{5}s_i \times v_i $$

Le problème du sac à dos respecte bien le principe d'optimalité. Considérons une solution optimale pour le problème à $n$ objets. Le $n$-ième objet peut être pris ou non dans la solution (le $5^{ème}$ objet fait partie d'une des solutions optimales proposées mais pas de l'autre). Distinguons les cas :

* si le $n$-ième objet ne fait pas partie de la solution alors le problème avec les $n-1$ premiers objets était déjà optimal
* si le $n$-ième objet fait partie de la solution, considérons la solution restreinte aux $n-1$ premiers objets et ménageant de la place pour placer le $n$-ième (il faudra le rajouter à la fin car il fait partie de la solution). Si cette sous-solution n'est pas optimale cela signifie qu'il est possible d'agencer les $n-1$ premiers objets plus efficacement. Dans ce cas l'ajout du dernier objet améliorera la solution ce qui est impossible car celle-ci est censée être optimale...

Le principe d'optimalité est donc respecté : *tout sous-sac extrait d'un sac optimal est optimal*.

Ce constat permet d'établir la relation de récurrence. Au moment d'étudier le $n$-ième objet nous avons deux situations : 

* soit il est intéressant de le placer dans le sac et donc il faut étudier un sous-sac avec les $n-1$ premiers objets et de poids maximal $W-w_n$ (on ménage de la place pour le $n$-ième objet) auquel on ajoutera la valeur $v_n$. Il faut toutefois s'assurer que $W-v_n$ est positif ou nul : par exemple, si un sac a un poids maximal de $3$ et que l'on manipule un objet de poids $5$, il est clair que cet objet ne fait pas partie d'une solution optimale
* soit ce n'est pas intéressant et donc on étudie le problèmes avec les $n-1$ premiers objets et de poids maximal $W$ (on ne cherche pas à ménager de la place)

Le choix entre ces deux situations se fera en conservant celle qui renverra la valeur maximale. Si l'on note $K(n,W)$ la valeur maximale du problème à $n$ objets et de poids maximal $W$, on a donc :

$$K(n, W) = max(\,K(n-1, W - w_n) + v_n\,,\,K(n-1, W)\,)$$

Le cas de base est atteint pour :

* $K(k,0)$ qui vaut $0$ (un sac à dos de poids total $0$ dans lequel on s'autorise à placer $0 \le k \le N$ objets a nécessairement une valeur de $0$)
* $K(0,w)$ qui vaut aussi $0$ (le problème du sac à dos à $0$ objets à une valeur optimale de $0$ quelle que soit le poids maximal $0 \le w \le W$)

La résolution du problème nécessite alors de calculer les valeurs de $K(n,w)$ pour les valeurs $0 \le n \le N$  et $0 \le w \le W$. On peut présenter ces valeurs dans un tableau (ici adapté à $N=5$ et $W=10$, ona rajouté les poids et valeurs des objets pour faciliter la lecture) :


|Poids/Nombre d'objets|0|1|2|3|4|5|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Poids|-|1|3|3|4|5|
|Valeur|-|2|1|3|5|3|
|0|0|0|0|0|0|0|
|1|0|-|-|-|-|-|
|2|0|-|-|-|-|-|
|3|0|-|-|-|-|-|
|4|0|-|-|-|-|-|
|5|0|-|-|-|-|-|
|6|0|-|-|-|-|-|
|7|0|-|-|-|-|-|
|8|0|-|-|-|-|-|
|9|0|-|-|-|-|-|
|10|0|-|-|-|-|-|


On a rempli la première ligne car elle correspond au premier cas de base. La première colonne correspond quant à elle au second cas de base.

Pour compléter le reste du tableau, on doit comparer les valeurs de deux cases de la colonne précédente (celle du problème avec $1$ objet de moins) :

* En premier lieu la valeur de la case située immédiatement à gauche. Cela correspond au cas où l'on ne prend pas l'objet $n$
* D'autre part la valeur de la case située $w_n$ lignes plus haut dans la case précédente (problème de taille $n-1$ dans lequel on réserve de la place pour l'objet $n$ : son poids est donc de $w-w_n$) à laquelle on ajoute la valeur de l'objet $n$

Encore une fois, cette seconde valeur ne peut être évaluée que si le poids du sac étudié est supérieur ou égal au poids du $n$-ième objet.

Le tableau compléter devient donc :

|Poids/Nombre d'objets|0|1|2|3|4|5|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Poids|-|1|3|3|4|5|
|Valeur|-|2|1|3|5|3|
|0|0|0|0|0|0|0|
|1|0|**2**|2|2|2|2|
|2|0|2|2|2|2|2|
|3|0|2|2|3|3|**3**|
|4|0|2|3|5|5|5|
|5|0|2|3|5|**7**|7|
|6|0|2|3|5|7|7|
|7|0|2|3|6|8|8|
|8|0|2|3|6|10|10|
|9|0|2|3|6|10|10|
|10|0|2|3|6|10|10|

Expliquons certains calculs :

* Pour la valeur $2$ de la ligne $1$ : la question est "quelle est la valeur optimale d'un sac de poids maximal de $1$ et n'utilisant que l'objet $1$ (de poids $1$ et de valeur $2$) ?". On compare la valeur de la case de gauche ($0$) et celle située $1$ ligne plus haut dans la colonne précédente (poids de l'objet $1$), valeur augmentée de $2$ (valeur de l'objet $1$). Le maximum de ces valeurs vaut $2$
* Pour la valeur $3$ de la ligne $3$ : la question est "quelle est la valeur optimale d'un sac de poids maximal de $3$ et utilisant les objets $1$ à $5$ ?". On compare la valeur de la case de gauche ($3$) et celle située $5$ lignes plus haut dans la colonne précédente. Or cette case n'existe pas ($3-5<0$). Donc on retient la valeur de gauche $3$
* Pour la valeur $7$ de la ligne $5$ : la question est "quelle est la valeur optimale d'un sac de poids maximal de $5$ et utilisant les objets $1$ à $4$ ?". On compare la valeur de la case de gauche ($3$) et celle située $4$ lignes plus haut dans la colonne précédente (l'objet $4$ pèse $4$), valeur à laquelle on ajoute la valeur de l'objet $4$. On compare donc $5$ (cas de de gauche) et $2+5=7$ (case du dessus augmentée de $v_4$). On retient la valeur $7$

La figure suivante permet de visualiser les cases intervenants dans les calculs de maximum : on retrouve la case de gauche (objet non pris) et une case située à gauche et $w_i$ lignes plus haut. On n'a pas reproduit l'ensemble du tableau.

![Illustration des calculs](images/Knapsack_array.png){width=30%}

Une fois le tableau complété, on peut trouver notre solution :

* on débute en bas à droite du tableau : cette case correspond à la valeur optimale de notre sac ($K(5,10) = 10$)
* on regarde la case située juste à gauche : si elle est égale, cela signifie que l'objet en cours (le $5$) n'a pas été retenu dans par l'algorithme
* on passe à cette case de gauche qui vaut $10$
* là encore on regarde la case de gauche : elle est différente donc l'objet $4$ a été retenu.
* on passe alors à la case située $4$ lignes plus haut dans la colonne précédente. En effet l'objet $4$ pèse $4$. Il s'agit du $5$ de la ligne $6$
* on recommence l'opération : comparaison avec la valeur de gauche, comme elle est différente, on retient l'objet $3$ puis on monte de $3$ cases ($w_3=3$)
* *etc*...

On retrouve bien notre solution $(1,0,1,1,0)$.

L'algorithme *Bottom-up* (itératif donc) peut être écrit ainsi :

```
Poids est le tableau contenant les poids des objets (indexé à partir de 1)
Valeurs est le tableau contenant les valeurs des objets (indexé à partir de 1)
N est le nombre d'objets
W est le poids maximal du sac
T est un tableau de W+1 lignes et N+1 colonnes

# Initialisation du tableau T
pour c de 0 à W faire
  T[c,0] := 0
fin pour
pour n de 0 à N faire
  T[0,n] := 0
fin pour

# Traitement, on traite chaque ligne (capacités)
# de chaque colonne (nombre d'objets)
pour i de 1 à N faire
  pour c de 0 à W faire
    si c-Poids[i] > =0 alors
      T[c,i] := max(T[c-Poids[i], i-1] + Valeurs[i], T[c,i-1])
    sinon
      T[c,i] := T[c,i-1]
    fin si
  fin pour
fin pour
```

1. Coder cet algorithme en python. Attention, la transposition est subtile car les listes python sont indexées à partir de `0`. On peut soit utiliser cette indexation et rejeter la colonne 0 à la fin du tableau (avec l'indice `-1`), soit ajouter un objet d'indice `0` dans les tableaux `Poids` et `Valeurs` avec des données absurdes (`-1` par exemple).

2. Coder la fonction `solution` qui prend en argument le tableau `T` et renvoie la solution du problème.

3. Adapter l'algorithme de résolution en une version *Top-down* (récursive donc) utilisant la mémoïsation.

Le tableau `T` généré dans cette approche récursive est incomplet :

|Poids/Nombre d'objets|0|1|2|3|4|5|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Poids|-|1|3|3|4|5|
|Valeur|-|2|1|3|5|3|
|0|0|0|0|0|0|0|0|
|1|0|0|2|2|2|0|0|
|2|0|0|2|2|0|0|0|
|3|0|0|2|2|0|0|0|
|4|0|0|2|0|0|0|0|
|5|0|0|2|3|5|7|0|
|6|0|0|2|3|5|0|0|
|7|0|0|2|3|0|0|0|
|8|0|0|0|0|0|0|0|
|9|0|0|0|0|0|0|0|
|10|0|2|3|6|10|10|

En effet, contrairement à l'approche itérative qui remplit de façon exhaustive toutes les cases en partant d'en haut à gauche, l'approche récursive "remonte" le tableau depuis la case en bas à droite et en se déplaçant vers la gauche et le haut (lorsque c'est possible). Seule les cases nécessaires aux calculs récursifs initiés par la dernière case sont remplies. Cette approche récursive est donc plus efficace dans ce problème

4. Écrire une code récursif n'utilisant pas la mémoïsation et comparer le nombres de valeurs calculées avec celui du code précédent.

*Remarques :*

* Dans le cas d'un problème à $N=15$ objets et pour un poids maximal $W=50$, un traitement récursif **sans** mémoïsation nécessite $56\,190$ appels récursifs
* **Avec** la mémoïsation on descend à $563$ !
* Le traitement **itératif** nécessite quant à lui de calculer l'ensemble des valeurs du tableau. Il y en a $(15+1)\times(50+1)=816$. Même en tenant compte du fait que la première ligne et la première colonne restent à la valeur $0$, cela laisse encore beaucoup de valeurs inutiles à calculer...
 
On comprend donc tout l'intérêt de la méthode récursive avec mémoïsation dans ce problème du sac à dos !
