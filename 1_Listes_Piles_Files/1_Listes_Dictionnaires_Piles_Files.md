---
title: |
    DIU Enseigner la spécialité NSI \
    Bloc 5 \
    - Tableaux, dictionnaires, liste, piles et files -

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhf{}
    \lhead{DIU Enseigner la spécialité NSI}
    \rhead{Tableaux, dictionnaires, liste, piles et files}
    \cfoot{\thepage}

author: |
    N. Revéret

lang:
- french

geometry:
- top=2.5cm
- bottom=2cm
- left=2cm
- right=2cm

output:
  pdf_document:
    fig_caption: yes
    table_caption: no
    df_print: kable
  html_document:
    fig_caption: yes
    df_print: paged
---

Le programme de terminale fait la part belle aux structures de données. Ce document vise à en présenter certaines :

* les listes python
* les tables de hachage
* les listes chaînées
* les piles et les files

Il est préférable d'avoir certaines bases de *programmation orientée-objet* avant d'aborder les aspects de programmation.

# Listes Python

Nous manipulons régulièrement les listes python en guise de "tableau", au sens informatique du terme. Rappelons à ce titre qu'un tel tableau est normalement de taille et de type finis : on crée par exemple un tableau de $500$ entiers, si l'on souhaite en manipuler $501$, il faut un nouveau tableau. Cette façon de travailler est par exemple utilisée par C.

L'intérêt de cette conception peut se faire sentir en termes de complexité. Il faut comprendre que lors de la création d'un tel tableau, une zone de mémoire **contiguë** est allouée au tableau. La variable tableau quant à elle pointe vers la première cellule du tableau.

![Un tableau en mémoire](images/str-ptr.jpg){width=50%}

Dès lors accéder à la cellule `i` du tableau peut se faire en temps constant : l'ordinateur connaît la position `p0` de la première cellule dans la mémoire ainsi que le type des objets de ce tableau. Il peut donc en déduire la `taille` de chaque cellule (dans la figure, les cellules font $1$ octet car les objets stockés sont des caractères ASCII). La cellule `i` se trouve donc à l'adresse `p0 + i * taille`. Cet accès se fait donc en temps constant et sa complexité est de $\mathcal{O}(1)$. 

Modifier une valeur dans le tableau se fait aussi en temps constant. La question de l'ajout d'une valeur quant à elle ne se pose pas car la taille du tableau est fixe.

 Les listes python sont sensiblement différentes. On décrira ici l'implémentation effectuée par Cpython.

 Une liste python est en fait un tableau dynamique codé en C. Sa taille est variable et adaptée au besoins de l'utilisateurs.

 Un tel tableau est représenté en C par :

 * un pointeur vers la première cellule du tableau
 * un tableau de pointeurs vers les valeurs effectives du tableau

Ce tableau de pointeurs est le plus souvent plus grand que la taille du tableau aux yeux de l'utilisateur de python : si celui-ci pense manipuler un tableau de taille $60$ en réalité, en C, un tableau de taille $72$ est réservé.

L'accès à une cellule précise reste en temps constant. La différence se fait lors de l'ajout de valeurs (`append`). Si notre tableau python compte $71$ valeurs et que l'on en ajoute $1$, on passe à $72$ ce qui ne pose pas de problème car le tableau en mémoire peut accueillir cette nouvelle valeur.

Par contre lors d'un nouvel ajout, on passe de $72$ à $73$ éléments et le tableau doit être entièrement reconstruit par l'ordinateur afin d'avoir une taille adéquate.

La nouvelle taille allouée (`allocate`) se calcule ainsi à partir de la nouvelle taille (`new_s`) du tableau (le $73$ dans notre exemple) :

    allocate = new_s >> 3
    Si new_s < 9 :
      allocate += 3
    Sinon :
      allocate += 6
    allocate += new_s

On rappelle que `new_s >> 3` correspond au décalage de `3` bits vers la droite de l'écriture binaire de `new_s`. Par exemple le nombre $13$ s'écrit en binaire `1101` et donc `13 >> 2 = 3` alors que `13 >> 3 = 1`.

1. Quelle est la nouvelle taille du tableau en mémoire si l'on doit passer de $72$ à $73$ ?

Cette opération nécessite de copier l'ensemble du tableau dans la nouvelle plage de mémoire allouée : sa complexité est donc de $\mathcal{O}(n)$. Par contre cette opération n'est pas effectuée à chaque ajout de valeur, seulement lorsque la taille limite est atteinte. On pourrait alors calculer le **coût amorti** de cette opération (coût moyen de l'ajout d'une valeur dans un tableau dont la taille varie de $0$ à $100$ par exemple).

Ce temps d'ajout supérieur à la normale s'observe lorsque l'on chronomètre les ajouts successifs.

![Temps d'ajouts](images/insertions_liste.png){width=70%}

2. (Si vous avez du temps !) Chronométrer votre ordinateur (avec `time.perf_counter_ns`) lors de $200$ ajouts de la valeur `1` dans un tableau initialement vide).

On pourra lire en complément l'excellent article de Laurent Luce sur l'implémentation des liste python : [https://www.laurentluce.com/posts/python-list-implementation/](https://www.laurentluce.com/posts/python-list-implementation/).

# Tables de hachage

Les tables de hachage sont utilisées en python fin d'implémenter les dictionnaires qui associent des `clés` à des `valeurs`.

![Clés et Valeurs](images/dict.png){width=40%}

Le principe est de transformer chaque valeur possible de `clé` en un indice de cellule dans la table de hachage. La cellule correspondant à une clé contient alors la `valeur` (ou le couple `clé`/`valeur`).

$$ Clé \rightarrow f(Clé) = Indice\,dans\,la\,table\,\rightarrow Valeur$$

![Table de hachage](images/hash_table.png){width=35%}

La "transformation" se fait à l'aide d'une **fonction de hachage**. Une telle fonction doit satisfaire certaines propriétés :

* deux objets identiques doivent renvoyer le même hash
* deux objets différents doivent renvoyer des hash différents (*cf infra*)
* les *hashs* doivent dans l'idéal être distribués uniformément dans l'ensemble des indices de la table de hachage

En python, les fonctions de hachage associées à des objets sont définies dans `objet.__hash__`. Bien souvent ces fonctions renvoient le résultat d'un calcul *modulo* une certaine valeur fixée par python. Cette valeur peut être liée au système ($32$ bits, $64$ bits ?) ou à la taille de la table de hachage dans le cas des dictionnaires.

Les fonctions de hachages sur les nombres entiers opèrent un "simple" *modulo* (c'est un peu plus subtil pour les décimaux, flottants et autres nombres rationnels). La fonction de hachage sur les chaînes de caractères effectue des calculs sur les adresses mémoires de chacun des caractères (avec des `XOR`) avant d'appliquer un *modulo*.

Les dictionnaires python sont créés initialement avec une table de hachage de longueur maximale $8$ (voir la partie sur l'implémentation des listes python). Ces tables sont redimensionnées lorsqu'elles sont remplies aux deux tiers). La fonction de hachage associée doit donc prendre n'importe quel objet (non mutable comme on le verra) et renvoyer un entier entre $0$ et $7$ (les calculs se font *modulo* $8$). On s'en doute certaines clés vont renvoyer le même *hash* : on appelle ce phénomène une collision. Deux solutions peuvent être envisagées :

* stocker une liste de couples clé/valeur dans les cellules de la table de hachage. Si deux *hashs* sont en collision on les place dans la même liste. Lorsque l'on souhaite accéder à la valeur associée à une certaine clé, le système trouve la bonne cellule puis parcourt la liste stockée jusqu'à trouver la bonne clé

![Résolution des collision à l'aide d'une liste](images/collision1.png){width=70%}

* Calculer un nouveau numéro de cellule à partir de la valeur du *hash* obtenu jusqu'à obtenir une cellule vide. C'est la solution adoptée par python. On peut par exemple, si une cellule est déjà occupée, essayer la suivante, puis la suivante de la suivante... Python procède différemment recalculant une nouvelle valeur.

![Résolution des collision par "rebonds"](images/collision2.png){width=30%}

*Remarque :* Les clés d'un d'un dictionnaire python doivent toujours, une fois *hachées*, pointer vers la même cellule. Or les fonctions de *hash* sont par définition très sensibles aux valeurs contenues dans l'objet qu'on leur propose. Pour cette raison il sera impossible d'utiliser un objet `list` comme clé d'un dictionnaire. Le caractère mutable de cet objet fait que son *hash* pourrait être modifié lors de l'exécution du code et donc la même variable de type `list` ne donnerait pas le même *hash* à deux instant de l'exécution.

On peut contourner ce soucis en transformant nos `list` en `tuple` qui sont par definition non-mutables :

```ipython
In [1]: dico = {}

In [2]: dico[[1]] = "essai"
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-2-4bbe958d401f> in <module>
----> 1 dico[[1]] = "essai"

TypeError: unhashable type: 'list'

In [3]: dico[(1,)] = "essai"

In [4]: dico[(1,)]
Out[4]: 'essai'
```

En termes de complexité, pour python, l'accès à une valeur à partir de sa clé est en temps constant (il "suffit" de calculer le *hash* de la clé et d'accéder à la cellule correspondante du tableau). L'ajout d'une valeur est en temps aussi en temps moyen constant (mais à l'instar des listes, certains ajouts peuvent être plus long lors du redimensionnement de la table de hachage).

Là encore on pourra lire [https://www.laurentluce.com/posts/python-dictionary-implementation/](https://www.laurentluce.com/posts/python-dictionary-implementation/).

En ce qui concerne les complexités en temps des différentes structures natives de python on consultera [https://wiki.python.org/moin/TimeComplexity](https://wiki.python.org/moin/TimeComplexity).

On fournit en complément de ce document une classe `Dictionnaire` implémentant une table de hachage rudimentaire en python.

# Type de données abstrait

Un type de données abstrait (*TDA*) est un modèle mathématique permettant de décrire des données. Afin de simplifier les choses, retenons qu'un *TDA* :

* décrit les attributs d'un objet
* décrit les opérations réalisables sur cet objet. Ces opérations sont définies à l'aide de **méthodes primitives**

L'interface d'un *TDA* est sa spécification, la description de ses éléments constitutifs et de ses méthodes primitives. On ne rentre pas dans le détail du code des méthodes, on se contente de donner leurs spécifications (entrées, rôle et sorties)

Le but de l'interface est donc de décrire le fonctionnement général d'un objet en précisant ce qu'il contient et comment le manipuler. Cette notion est à distinguer de l'*implémentation* d'un objet : cette notion décrit comment l'objet est codé dans le langage, comment il est construit. Par exemple :

* l'interface d'une voiture est globalement constante : un volant pour contrôler la direction, trois pédales pour embrayer, freiner et accélérer
* l'implémentation de la voiture dépend du modèle, du fabriquant. En tant qu'utilisateur de base, je n'ai pas besoin de connaître le fonctionnement de la pédale de frein (implémentation). Tout ce que je dois savoir c'est que quand j'appuie sur la pédale du milieu, ma voiture freine (interface)

![L'interface est la partie émergée de l'iceberg](images/interface-vs-implementation.png){width=20%}

# Listes chaînées

Une liste chaînée est une structure de donnée permettant de stocker plusieurs valeurs. La longueur de la liste n'est pas fixée *a priori* (ce qui la distingue d'un tableau) et peut évoluer au fil des ajouts et suppressions d'éléments.

Chaque élément, ou maillon, $x$ d'une liste chaînée comporte deux attributs :

* la valeur à stocker $x.donnée$
* un lien vers le prochain élément $x.successeur$ : un élément contient un autre élément ! On peut ainsi se représenter la structure comme des éléments reliés le long d'une chaîne

La liste chaînée quant à elle contient au minimum un attribut, la **tête de la liste** $L.tête$ qui est le premier maillon(donnée + successeur). La liste vide est telle que sa tête contient `null` (elle pointe vers un élément nul).

![Liste chaînée](images/liste_chainee.png){width=60%}

Le successeur du dernier élément de la liste vaut lui aussi `null` (il ne pointe nulle part).

Du point de vue des primitives, les méthodes minimales à implémenter sont :

* la création d'une liste vide
* l'insertion d'une valeur, sous la forme d'un maillon, dans la liste (au début ou à un indice précis)
* la suppression du premier élément. Cette fonction renvoie la valeur de ce premier élément

On peut aussi ajouter les méthodes suivantes :

* calcul de la longueur de la liste : le nombre de maillons
* itération sur la liste afin de parcourir les maillons dans une boucle "Pour"
* suppression d'un maillon désigné par son indice

## Insertion dans une liste chaînée

Lors de l'insertion dans une liste chaînée, on doit distinguer deux cas :

* on souhaite insérer la valeur en tête de liste. Dans ce cas on crée un nouvel élément et on fait pointer son successeur vers l'ancienne tête de la liste. Ensuite on désigne cet élément comme la tête de la liste

![Insertion en début de liste chaînée](images/lc_insertion_debut.jpg){width=70%}

* on souhaite insérer l'élément à l'indice `i` (inférieur à la longueur de la liste). On parcourt la liste jusqu'à arriver à l'élément d'indice `i-1` : le nouvel élément devra être le nouveau successeur de celui-ci. L'ancien successeur devient le successeur du nouvel élément

![Insertion à l'intérieur de la liste chaînée](images/lc_insertion_interieur.jpg){width=70%}

## Suppression dans une liste chaînée

Lors de la suppression, on se contente de sauter l'élément à éliminer dans la suite des successeurs. Si l'on souhaite par exemple supprimer l'élément d'indice `i`, on fait en sorte que l'élément d'indice `i-1` pointe vers l'élément d'indice `i+1`

![Suppression dans une liste chaînée](images/lc_del.png){width=70%}

Dans le cas ou on souhaite supprimer l'élément d'indice `0`, il ne faut pas oublier de faire pointer la tête de la liste vers l'ancien successeur de la tête.

1. Compléter le fichier `liste_chainee.py` afin d'obtenir une liste chaînée fonctionnelle

## Autres listes chaînées

Il est possible de construire d'autres types abstraits sur le modèle des listes chaînées :

* les listes chaînées circulaires : le successeur du dernier élément pointe vers la tête. Pratique pour le [problème de *Josèphe*](https://fr.wikipedia.org/wiki/Probl%C3%A8me_de_Jos%C3%A8phe)

![Liste chaînée circulaire](images/circularlink.png){width=70%}

* les listes doublement chaînées : chaque élément garde trace de son prédécesseur. La liste peut être lue dans les deux 

![Liste doublement chaînée](images/doubly.png){width=70%}


# Les Piles

## Définition et utilisation

Une ***Pile***, ***Stack*** en anglais (comme [Stack Overflow !](https://stackoverflow.com/)), est une structure de données reposant sur le modèle du ***Last In, First Out*** (***LIFO***), ou ***Dernier Entré, Premier Sorti*** (***DEPS***) en français.

La façon la plus simple de se représenter une *pile* est une pile (!) d'assiettes : lorsqu'on les range, on empile les assiettes les unes sur les autres. Lorsque l'on met la table, on pioche dans la pile en commençant par le dessus et donc par la dernière assiette rangée !

![Pile d'assiette](images/pile_assiette.png){width=50%}

![Pile en programmation](images/Stack.png){width=50%}

On le voit, la *pile* en soit n'a donc qu'un seul attribut : sa **tête**, son premier élément.

La *pile* intervient très souvent dans la vie quotidienne et en programmation :

* la façon dont un professeur pioche la prochaine copie à corriger (le dernier élève à rendre est le premier corrigé !)
* la gestion d'un camion de livraison : on charge dans l'ordre inverse de la livraison, le dernier carton chargé est le premier livré
* le `ctrl-Z` de l'ordinateur
* la gestion de l'historique de navigation sur internet
* déterminer si une expression compte le bon nombre de parenthèses (on empile les ouvrantes et dépile les fermantes, voir plus bas)
* parcours en profondeur d'abord d'arbres et de graphes (version itérative)
  
## Implémentation

Les méthodes primitives essentielles d'une *pile* sont :

* `creation` : crée et retourne une *pile* vide
* `est_vide` : indique si la *pile* est vide ou non
* `empiler` : ajoute un élément en haut de la *pile*. Les anglo-saxons disent *push*
* `depiler` : retire, et retourne, le premier élément de la *pile*. Les anglo-saxons disent *pop*

Il est aussi possible d'ajouter les méthodes suivantes mais qui ne sont pas indispensables :

* `taille` : retourne le nombre d'éléments dans la *pile*
* `tete` : retourne le premier élément sans le dépiler
* `vider` : dépile tous les éléments

Il existe plusieurs implémentations des *piles* :

* On peut utiliser des tableaux (`list` python). Dans ce cas on se contente pour un empilement d'ajouter une valeur en fin de liste. Pour un dépilement, on effectue un `pop`

1. Compléter le fichier `pile_fonctionnelle.py` fournissant une implémentation des piles respectant le paradigme de la programmation fonctionnelle.

* Il est aussi possible d'utiliser des listes chaînées. Le fichier `pilesFiles.py` propose une telle implémentation en programmation orientée-objet

## Application

Une pile permet facilement de vérifier si une suite de parenthèses est bien formée. Par exemple la chaîne `(()())` est bien formée alors que la chaîne `(()` ne l'est pas.

1. Coder dans le fichier `parentheses.py` une fonction `parentheses_correctes` prenant en argument une chaîne de parenthèses (seulement des `"("` ou `")"`, pas de crochets) et renvoyant `True` si elle est bien formée, `False` dans le cas contraire.

La notation polonaise inversée a été inventée par Jan Łukasiewicz en 1924. Il s'agit d'écrire les expressions mathématiques en plaçant l'opérateur (le symbole d'opération) après les opérandes (les nombres ou variables) et non entre eux. On parle aussi de notation *suffixe* (ou *postfixe*) là où notre façon classique d'écrire les calculs est une notation *infixe*.

*Remarque :* en réalité, Łukasiewicz a inventé la notation *préfixe*, l'opérateur est placé avant les opérandes. On parle de notation polonaise (son nom est-il trop compliqué à prononcer ?). D'où l'expression "notation polonaise inversée" pour la notation *postfixe*.

Par exemple le calcul $3+5\times 2$ devient en notation *postfixe* $5\,2\,\times\,3\,+$.

L'un des grands avantages de cette notation est de régler les problèmes de priorités opératoires : dans un calcul en notation *postfixe* il suffit d'effectuer les opérations de gauche à droite !

2. L'évaluation (le calcul) d'une expression *postfixe* peut se faire à l'aide d'une pile. Dans le fichier `calculette.py`, coder une telle fonction `evaluation_postfixe` qui prend en argument une chaîne de caractères correspondant à un calcul en *postfixe* et renvoie son résultat. On convient que dans la chaîne, chaque *item* (nombre ou opérande) est séparé par un "espace".

La conversion d'une expression *infixe* en *postfixe* peut se faire à l'aide de l'algorithme [*Shunting Yard*](https://fr.wikipedia.org/wiki/Algorithme_Shunting-yard) (algo. de la gare de triage).

3. (Si vous avez le temps) Coder cet algorithme dans la fonction `in_to_post` du fichier `calculette.py`.


# Les Files

## Définition et utilisation

Une ***File***, ***Queue*** en anglais, est une structure de données reposant sur le modèle du ***First In, First Out*** (***FIFO***), ou ***Premier Entré, Premier Sorti*** (***PEPS***) en français.

La façon la plus simple de se représenter une *file* est la queue à un guichet : la première personne arrivée sera la première servie.

![File d'attente](images/queue.png)

![File d'éléments](images/Data_Queue.png)

La *file* a deux attributs : une **tête** (le premier élément, prochain à sortir) et une **queue** (dernier élément qui vient d'être inséré).

Voici des exemple d'utilisation de *files* :

* file d'attente
* gestion des appels dans un standard
* gestion des tâches par le processeur
* tri des paquets lors d'une transmission asynchrone (les paquets des différents messages arrivent dans le désordre, ils sont réassemblés au fur et à mesure dans leur ordre d'arrivée)
* parcours en largeur d'abord d'arbres et de graphes

## 2. Implémentation

Les méthodes primitives essentielles d'une *file* sont :

* `creation` : crée et retourne une *file* vide
* `est_vide` : indique si la *file* est vide ou non
* `enfiler` : ajoute un élément à la queue de la *file*. Les anglo-saxons disent *enqueue*
* `defiler` : retire, et retourne, le premier élément de la *pile*. Les anglo-saxons disent *dequeue*

Là encore, il est possible d'utiliser différentes implémentations :

* A l'aide de listes python
* A l'aide de listes chaînées
* A l'aide de deux piles !

![File comme deux piles](images/file_as_stacks1.png){width=40%}

![File comme deux piles](images/file_as_stacks2.png){width=40%}