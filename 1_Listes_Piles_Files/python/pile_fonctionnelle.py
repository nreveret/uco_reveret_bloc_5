def pile_vide() :
    """
    Renvoie une pile vide
    """
    pass

def est_vide(pile) :
    """
    Renvoie True ou False selon que la pile est vide ou non
    """
    pass

def empiler(pile, valeur) :
    """
    Empile la valeur dans la pile et renvoie la pile
    """
    pass

def depiler(pile) :
    """
    Dépile et renvoie la valeur de tête de la pile (la dernière valeur ajoutée)
    """
    pass

def afficher(pile) :
    """
    Affiche le contenu de la pile (le dernier élément ajouté est à droite)
    """
    print(pile)

if __name__ == "__main__" :
    # Création d'une pile vide
    p = pile_vide()
    print("Une pile vide :")
    afficher(p)

    # Ajout de deux valeurs
    empiler(p, "1er ajout")
    empiler(p, "2ème ajout")
    print("La pile après deux ajouts :")
    afficher(p)

    # Dépilement
    print("Le résultat d'un dépilement :")
    print(depiler(p))
    print("La pile après le dépilement :")
    afficher(p)