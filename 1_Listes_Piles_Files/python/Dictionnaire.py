class Dictionnaire:
    """
    Classe implémentant un dictionnaire en tant que table de hachage
    Le dictionnaire comporte 256 hash au maximum
    S'il y a collision entre les hash des clés, on liste les couples (clé, valeur) à la suite
    les uns des autres
    """
    def __init__(self, size = 256):
        """
        Constructeur
        Crée une table de hachage de size (256 par défaut) éléments maximums (pas de redimensionnement)
        On peut toutefois stocker plus de valeurs en les empilant dans une liste 
        """
        self.size = size
        self.hashmap = [[] for _ in range(self.size)]
        

    def hash_func(self, key):
        """
        Calcule le hash de la clé % la taille du dictionnaire (256 par défaut)
        """
        hashed_key = hash(key) % self.size
        return hashed_key

    def set(self, key, value):
        """
        Associe une valeur à une clé
        S'il y a collision entre les hash des clés, on liste les couples (clé, valeur) à la suite
        les uns des autres
        """
        print(f"Le hash python de la clé : {hash(key)}")
        hash_key = self.hash_func(key)
        print(f"Le hash % taille du dictionnaire de la clé (indice de la cellule) : {hash_key}")
        key_exists = False
        slot = self.hashmap[hash_key]
        for i, kv in enumerate(slot):
            k, v = kv
            if key == k:
                key_exists = True
                break

        if key_exists:
            slot[i] = (key, value)
        else:
            slot.append((key, value))

    def get(self, key):
        """
        Renvoie la valeur associée à la clé
        """
        hash_key = self.hash_func(key)
        slot = self.hashmap[hash_key]
        for (k, v) in slot:
            if key == k:
                return v
        raise KeyError('Key does not exist.')

    def __setitem__(self, key, value):
        """
        Associe une valeur à une clé
        """
        return self.set(key, value)

    def __getitem__(self, key):
        """
        Renvoie la valeur associée à la clé
        """
        return self.get(key)

    def __repr__(self) :
        s = "{ "
        
        for kv in self.hashmap :
            if kv :
                for k,v in kv :
                    s += str(k) + " : " + str(v) + " , "
        
        s = s[:-3] + " }"
        
        return s
    
    def __str__(self) :
        return self.__repr__()

if __name__ == "__main__" :

    # Création d'un dictionnaire de taille 8
    dico = Dictionnaire(8)

    # Ajout de deux valeurs
    dico.set("clé1", "valeur1")
    dico["clé2"] = "valeur2"

    print(dico.get("clé1"))
    print(dico["clé2"])

    # Notez comment la table diffèrent d'une exécution du code à l'autre
    # les fonctions de hash python sont "salées"
    print(dico.hashmap)