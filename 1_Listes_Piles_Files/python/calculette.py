from pilesFiles import Element, Pile
from math import pow, sqrt


def eval_postfixe(chaine: str) -> float:
    """
    Evalue la chaîne postfixe passée en argument

    chaine : est un calcul postfixe (str). Chaque item est séparé par un espace

    Renvoie le résultat du calcul (int ou float) 
    """
    pass


def in_to_post(chaine: str) -> list:
    """
    Traduit un calcul infixe en postfixe à l'aide de l'algorithme 
    de la gare de triage (shunting-yard)

    chaine : un calcul en infixe (str)

    Renvoie une liste contenant les éléments en postfix
    """

    pass
    

if __name__ == "__main__":
    ###################################################################
    # Test évaluation postfixe
    calcul = "2 3 +"
    print(f"Essai avec : {calcul}")
    print("(c'est à dire 2 + 3)")
    print(eval_postfixe(calcul))

    calcul = "12 15 3 2 * - 5 * +"
    print(f"Essai avec : {calcul}")
    print("(c'est à dire 12 + (15-3*2) * 5)")
    print(eval_postfixe(calcul))


    ###################################################################
    # Test conversion infixe -> postfixe
    expression = "12 + (15-3*2) * 5"

    print(f"L'expression infixe à calculer : {expression}")
    postfixe = in_to_post(expression)
    print(f"L'expression postfixe à calculer : {' '.join(postfixe)}")
