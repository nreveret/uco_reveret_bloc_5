from time import perf_counter_ns
import matplotlib.pyplot as plt
from statistics import mean

def calcule_tailles(t_max) :
    """
    Calcule les tailles des "tableaux" créés en mémoire par python
    Une copie de la liste sera effectuée à chaque append lorsque la liste
    atteint l'une de ces tailles
    """
    sizes = [0]
    while sizes[-1] < t_max :
        new_s = sizes[-1] + 1
        new_alloc = (new_s >> 3) + (new_s < 9) * 3 + (new_s >= 9) * 6 + new_s
        sizes.append(new_alloc)
    return sizes[:-1]

def moyenne_colonne(liste, indice) :
    """
    Calcule la valeur moyenne de la colonne "indice" de la liste de listes
    """
    return mean([l[indice] for l in liste])

# Le nombre de répétitions des insertions (pour moyenner les temps)
nb_repetitions = 50

# Le nombre d'insertions réalisées
nb_insertions = 500
valeurs = [k for k in range(nb_insertions)]

durees = []

for k in range(nb_repetitions) :
    liste = []
    d = []
    for i in range(nb_insertions):
        t1 = perf_counter_ns()
        liste.append(1)
        t2 = perf_counter_ns()
        d.append(t2-t1)
    durees.append(d)

durees = [moyenne_colonne(durees, i) for i in range(nb_insertions)]

fig, ax = plt.subplots()
ax.set_xlabel('Taille de la liste')
ax.set_ylabel('Temps (ns)')

ax.bar(valeurs, durees, label="Temps en ns")

# Mise en évidence des insertions ayant entraînées un "resize"
resizes = calcule_tailles(nb_insertions)#[0, 4, 8, 16, 25, 35, 46, 58, 72, 88,...]
resizes_y = [durees[k] for k in resizes]
ax.plot(resizes, resizes_y, "rx", label="\"Resize\" de la liste")

ax.set_title("Temps d'insertions à la fin d'une liste en fonction de la taille")
ax.legend()
plt.show()

#################################################
# Illustrations des id dans le tableau

nb_insertions = 300
valeurs = [k for k in range(nb_insertions)]

ids = []
liste = []
for i in range(nb_insertions) :
    liste.append(i)
    ids.append(id(liste[-1])//10**6)

fig, ax = plt.subplots()
ax.set_xlabel('Taille de la liste')
ax.set_ylabel('Ids des pointeurs vers les valeurs')

depart = 0


ax.plot(valeurs[depart:], ids[depart:],"xb", label="Ids")

ax.set_title("Ids des pointeurs vers les valeurs de la liste")
plt.show()
