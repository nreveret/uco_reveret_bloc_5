from pilesFiles import *

def parentheses_correctes(chaine : str) -> bool :
    """
    Vérifie qu'une chaîne de parenthèses est bien formée et renvoie True ou False selon le cas
    """
    p = Pile()

    for token in chaine :
        if token == "(" :
            p.empiler(1)
        elif token == ")" :
            if not p.est_vide() :
                p.depiler()
            else :
                return False
    
    return p.est_vide()

if __name__ == "__main__" :
    chaine = "((())()(()))"
    print(f"Essai avec {chaine}")
    print(parentheses_correctes(chaine))

    chaine = "((())()(())"
    print(f"Essai avec {chaine}")
    print(parentheses_correctes(chaine))

    chaine = "((())()()))"
    print(f"Essai avec {chaine}")
    print(parentheses_correctes(chaine))