class Element:
    """
    Classe décrivant les éléments utilisés dans les listes chaînées
    Un élément possède :
        - une donnée (de type quelconque)
        - un successeur (un autre élément)
    """

    def __init__(self, donnee):
        self.donnee = donnee
        self.successeur = None

    def afficher(self):
        print(str(self.donnee) + " --> " + str(self.successeur), end="\n\n")


class Liste_Chainee:
    """
    Classe décrivant une liste simplement chaînée
    La liste est caractérisée par sa seule tête
    """

    def __init__(self):
        """
        Constructeur : renvoie la liste vide
        """
        self.tete = None

    def longueur(self):
        """
        Renvoie le nombre d'éléments dans la liste
        """
        l = 0
        courant = self.tete
        while courant != None:
            l += 1
            courant = courant.successeur
        return l

    def inserer(self, valeur, indice=0):
        """
        Insertion d'une valeur à l'indice indiqué (0 par défaut)
        """

        nouvel_element = Element(valeur)
        if indice == 0:
            nouvel_element.successeur = self.tete
            self.tete = nouvel_element
        else:
            assert indice < self.longueur(), "L'indice dépasse la taille de la liste"
            i = 0
            courant = self.tete
            while i+1 < indice:
                courant = courant.successeur
                i += 1
            nouvel_element.successeur = courant.successeur
            courant.successeur = nouvel_element

    def supprimer(self, indice):
        """
        Suppression de l'élément désigné par son indice
        """
        assert indice < self.longueur(), "Indice hors de la liste"

        if indice == 0:
            self.tete = self.tete.successeur
        else:
            i = 0
            courant = self.tete
            while i+1 < indice:
                courant = courant.successeur
                i += 1
            courant.successeur = courant.successeur.successeur

    def afficher(self):
        """
        Affichage de la liste
        """
        elts = []

        courant = self.tete

        while courant != None:
            elts.append(str(courant.donnee))
            courant = courant.successeur

        print("[" + ", ".join(elts) + "]")

    def __repr__(self):
        elts = []

        courant = self.tete

        while courant != None:
            elts.append(str(courant.donnee))
            courant = courant.successeur

        return "[" + ", ".join(elts) + "]"

    def __str__(self):
        return self.__repr__()

    def __iter__(self):
        courant = self.tete
        while courant != None:
            yield courant.donnee
            courant = courant.successeur


if __name__ == "__main__":
    # Création d'une liste vide
    lc = Liste_Chainee()
    print("Création d'une liste vide")
    lc.afficher()

    # Insertion en tête de liste
    print("Insertion en tête de liste")
    lc.inserer(1)
    lc.afficher()
    # Insertion en tête de liste
    print("Insertion en tête de liste")
    lc.inserer(2)
    lc.afficher()
    # Insertion à l'indice 1
    print("Insertion à l'indice 1")
    lc.inserer(3, 1)
    lc.afficher()
    # Insertion à l'indice 1
    print("Insertion à l'indice 1")
    lc.inserer(4, 1)
    lc.afficher()

    # La longueur de la liste
    print("La longueur de la liste")
    print(lc.longueur())

    # Suppression de l'élément de tête
    print("Suppression de l'élément de tête")
    lc.supprimer(0)
    lc.afficher()

    # Suppression de l'élément à l'indice 1
    print("Suppression de l'élément à l'indice 1")
    lc.supprimer(1)
    lc.afficher()

    # Itération
    print("Itération")
    for elt in lc:
        print(elt)
