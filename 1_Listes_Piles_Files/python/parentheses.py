from pilesFiles import *

def parentheses_correctes(chaine : str) -> bool :
    """
    Vérifie qu'une chaîne de parenthèses est bien formée et renvoie True ou False selon le cas
    """
    pass


if __name__ == "__main__" :
    chaine = "((())()(()))"
    print(f"Essai avec {chaine}")
    print(parentheses_correctes(chaine))

    chaine = "((())()(())"
    print(f"Essai avec {chaine}")
    print(parentheses_correctes(chaine))

    chaine = "((())()()))"
    print(f"Essai avec {chaine}")
    print(parentheses_correctes(chaine))